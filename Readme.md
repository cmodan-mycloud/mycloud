# Ports
1. Identity:                HTTPS:5000 HTTP:5001
2. Download:                HTTPS:5002 HTTP:5003
3. MetaData:                HTTPS:5004 HTTP:5005
4. Sharing:                 HTTPS:5006 HTTP:5007
5. Upload:                  HTTPS:5008 HTTP:5009
6. NotificationHub:         HTTPS:5010 HTTP:5011