﻿using System.Collections.Generic;

namespace Services.Common.Requests.Common
{
    public class ErrorResponse
    {
        public ErrorResponse(int errorCode, IEnumerable<string> errors)
        {
            ErrorCode = errorCode;
            Errors = errors;
        }

        public int ErrorCode { get; }
        public IEnumerable<string> Errors { get; }
    }
}