﻿using System;

namespace Services.Common.Requests.DTOs
{
    public class FolderDto
    {
        public FolderDto()
        {
        }

        public FolderDto(Guid id, string name, Guid ownerId, Guid parentId, DateTime createdOn)
        {
            Id = id;
            Name = name;
            OwnerId = ownerId;
            ParentId = parentId;
            CreatedOn = createdOn;
        }

        public Guid Id { get; set; }
        public string Name { get; }
        public Guid OwnerId { get; }
        public Guid ParentId { get; }
        public DateTime CreatedOn { get; }
    }
}