﻿using System;
using Newtonsoft.Json;
using Services.Common.Requests.DTOs;
using Services.Common.Requests.Interfaces.Event;

namespace Services.Common.Requests.Events
{
    public class FileMetaDataSavedEvent : IEvent
    {
        [JsonConstructor]
        public FileMetaDataSavedEvent(
            Guid id,
            string extension,
            string name,
            FolderDto folder,
            long sizeBytes,
            Guid ownerId,
            DateTime now
        )
        {
            Id = id;
            Extension = extension;
            Name = name;
            Folder = folder;
            SizeBytes = sizeBytes;
            OwnerId = ownerId;
            Now = now;
        }

        public Guid Id { get; }
        public string Extension { get; }
        public string Name { get; }
        public FolderDto Folder { get; }
        public long SizeBytes { get; }
        public Guid OwnerId { get; }
        public DateTime Now { get; }
    }
}