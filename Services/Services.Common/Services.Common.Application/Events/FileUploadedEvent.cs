﻿using System;
using Newtonsoft.Json;
using Services.Common.Requests.Interfaces.Event;

namespace Services.Common.Requests.Events
{
    public class FileUploadedEvent : IEvent
    {
        [JsonConstructor]
        public FileUploadedEvent(Guid ownerId, string fileName, string newFileName, string extension, long sizeInBytes,
            Guid folderId)
        {
            OwnerId = ownerId;
            FileName = fileName;
            NewFileName = newFileName;
            Extension = extension;
            SizeInBytes = sizeInBytes;
            FolderId = folderId;
        }

        public Guid OwnerId { get; }
        public string FileName { get; }
        public string NewFileName { get; }
        public string Extension { get; }
        public long SizeInBytes { get; }
        public Guid FolderId { get; }
    }
}