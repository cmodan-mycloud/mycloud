﻿using System;
using Services.Common.Requests.Interfaces.Event;

namespace Services.Common.Requests.Events
{
    public class PhotoUploadedEvent : IEvent
    {
        public PhotoUploadedEvent(string name, long sizeInBytes, string extension, Guid ownerId, string diskName)
        {
            Name = name;
            SizeInBytes = sizeInBytes;
            Extension = extension;
            OwnerId = ownerId;
            DiskName = diskName;
        }

        public string Name { get; }
        public long SizeInBytes { get; }
        public string Extension { get; }
        public Guid OwnerId { get; }
        public string DiskName { get; }
    }
}