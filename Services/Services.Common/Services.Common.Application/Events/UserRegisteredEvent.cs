﻿using System;
using Services.Common.Requests.Interfaces.Event;

namespace Services.Common.Requests.Events
{
    public class UserRegisteredEvent : IEvent
    {
        public UserRegisteredEvent(Guid userId)
        {
            UserId = userId;
        }

        public Guid UserId { get; }
    }
}