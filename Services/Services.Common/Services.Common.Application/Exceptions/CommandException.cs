﻿using System;
using Newtonsoft.Json;

namespace Services.Common.Requests.Exceptions
{
    public class CommandException : CqrsException
    {
        [JsonConstructor]
        public CommandException(string friendlyMessage, Exception inner) : base(friendlyMessage, inner)
        {
        }

        [JsonConstructor]
        public CommandException(string friendlyMessage) : base(friendlyMessage)
        {
        }
    }
}