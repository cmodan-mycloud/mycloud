﻿using System;
using System.Collections.Generic;

namespace Services.Common.Requests.Exceptions
{
    public abstract class CqrsException : Exception
    {
        protected CqrsException(string message) : base(message)
        {
            Errors = new List<string>();
        }

        protected CqrsException(string message, Exception innerException) : base(message, innerException)
        {
            Errors = new List<string>();
        }

        public IEnumerable<string> Errors { get; set; }
    }
}