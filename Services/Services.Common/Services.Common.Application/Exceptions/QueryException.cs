﻿using System;
using Newtonsoft.Json;

namespace Services.Common.Requests.Exceptions
{
    public class QueryException : CqrsException
    {
        [JsonConstructor]
        public QueryException(string friendlyMessage, Exception inner) : base(friendlyMessage, inner)
        {
        }

        [JsonConstructor]
        public QueryException(string friendlyMessage) : base(friendlyMessage)
        {
        }
    }
}