﻿using System.Threading.Tasks;

namespace Services.Common.Requests.Interfaces.Command
{
    public interface ICommandHandler<in T> where T : ICommand
    {
        Task HandleAsync(T command);
    }
}