﻿using System.Threading.Tasks;

namespace Services.Common.Requests.Interfaces.Event
{
    public interface IEventHandler<in T> where T : IEvent
    {
        Task HandleAsync(T @event);
    }
}