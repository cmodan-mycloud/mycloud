﻿using System.Threading;
using System.Threading.Tasks;

namespace Services.Common.Requests.Interfaces.IO
{
    public interface IExtractExtension
    {
        public Task<string> ExtractExtensionFrom(string fileName, CancellationToken cancellationToken);
    }
}