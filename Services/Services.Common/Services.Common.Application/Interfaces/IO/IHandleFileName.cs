﻿namespace Services.Common.Requests.Interfaces.IO
{
    public interface IHandleFileName
    {
        public string ExtractName(string fileName);
    }
}