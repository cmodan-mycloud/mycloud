﻿using System.Threading;
using System.Threading.Tasks;

namespace Services.Common.Requests.Interfaces.IO
{
    public interface IManipulatePaths
    {
        public Task<string> Combine(string first, string second, CancellationToken cancellationToken);
        public Task<bool> FileExists(string filePath, CancellationToken cancellationToken);
        public Task<bool> DirectoryExists(string directoryPath, CancellationToken cancellationToken);
        public Task CreateDirectory(string directoryPath, CancellationToken cancellationToken);
    }
}