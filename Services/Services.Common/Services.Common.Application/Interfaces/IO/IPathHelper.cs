﻿using System.Threading;
using System.Threading.Tasks;
using Services.Common.Requests.Interfaces.IO.Models;

namespace Services.Common.Requests.Interfaces.IO
{
    public interface IPathHelper
    {
        Task<string> GetOnBehalfOf(DirectoryPathModel model, CancellationToken cancellationToken);
    }
}