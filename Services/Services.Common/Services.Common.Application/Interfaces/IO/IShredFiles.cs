﻿using System.Threading;
using System.Threading.Tasks;

namespace Services.Common.Requests.Interfaces.IO
{
    public interface IShredFiles
    {
        public Task Shred(string directory, string fileName, CancellationToken cancellationToken);
    }
}