﻿using System;

namespace Services.Common.Requests.Interfaces.IO.Models
{
    public class DirectoryPathModel
    {
        public DirectoryPathModel(Guid ownerId)
        {
            OwnerId = ownerId;
        }

        public Guid OwnerId { get; }
    }
}