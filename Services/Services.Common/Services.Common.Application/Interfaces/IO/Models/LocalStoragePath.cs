﻿namespace Services.Common.Requests.Interfaces.IO.Models
{
    public class LocalStoragePath
    {
        public string StorageDirectoryName { get; set; }
        public string UnixEnv { get; set; }
        public string WinEnv { get; set; }
    }
}