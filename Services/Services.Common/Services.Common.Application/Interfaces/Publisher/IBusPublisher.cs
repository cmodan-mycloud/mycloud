﻿using System.Threading.Tasks;
using Services.Common.Requests.Interfaces.Command;
using Services.Common.Requests.Interfaces.Event;

namespace Services.Common.Requests.Interfaces.Publisher
{
    public interface IBusPublisher
    {
        Task SendAsync<TCommand>(TCommand command)
            where TCommand : ICommand;

        Task PublishAsync<TEvent>(TEvent @event)
            where TEvent : IEvent;
    }
}