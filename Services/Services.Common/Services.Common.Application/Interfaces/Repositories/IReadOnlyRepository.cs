﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Common.Requests.Interfaces.Repositories
{
    public interface IReadOnlyRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAllAsync(
            CancellationToken cancellationToken,
            Func<IQueryable<T>, IQueryable<T>> sharper = null
        );

        Task<T> GetByConditionAsync(
            CancellationToken cancellationToken,
            Expression<Func<T, bool>> expression,
            Func<IQueryable<T>, IQueryable<T>> sharper = null
        );

        Task<IEnumerable<T>> GetAllByConditionAsync(
            CancellationToken cancellationToken,
            Expression<Func<T, bool>> expression,
            Func<IQueryable<T>, IQueryable<T>> sharper = null
        );
    }
}