﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Common.Requests.Interfaces.Repositories
{
    public interface IWriteOnlyRepository<in T> where T : class
    {
        Task Create(T entity, CancellationToken cancellationToken);

        Task Update(T entity, CancellationToken cancellationToken);
        Task UpdateRange(IEnumerable<T> entities, CancellationToken cancellationToken);

        Task Delete(T entity, CancellationToken cancellationToken);
        Task DeleteRange(IEnumerable<T> entities, CancellationToken cancellationToken);
    }
}