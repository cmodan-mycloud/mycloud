﻿using System.Threading.Tasks;

namespace Services.Common.Requests.Interfaces.Seeding
{
    public interface IDatabaseSeeder
    {
        public Task Seed();
    }
}