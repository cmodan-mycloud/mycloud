﻿using Services.Common.Requests.Interfaces.Command;
using Services.Common.Requests.Interfaces.Event;

namespace Services.Common.Requests.Interfaces.Subscriber
{
    public interface IBusSubscriber
    {
        IBusSubscriber SubscribeCommand<TCommand>()
            where TCommand : ICommand;

        IBusSubscriber SubscribeEvent<TEvent>()
            where TEvent : IEvent;
    }
}