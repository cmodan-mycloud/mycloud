﻿using System.Threading;
using System.Threading.Tasks;

namespace Services.Common.Requests.Interfaces.Utilities
{
    public interface ICreateUniqueName
    {
        public Task<string> Create(string tail, CancellationToken cancellationToken);
    }
}