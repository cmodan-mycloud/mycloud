﻿namespace Services.Common.Requests.Interfaces.Utilities
{
    public interface IGuessPlatform
    {
        public Platform Guess();
    }

    public enum Platform
    {
        UnixBased,
        Windows
    }
}