﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Services.Common.Requests.Exceptions;

namespace Services.Common.Requests.Middlewares
{
    public class QueryCommandExceptionMiddleware : IMiddleware
    {
        private readonly ILogger<QueryCommandExceptionMiddleware> _logger;

        public QueryCommandExceptionMiddleware(ILogger<QueryCommandExceptionMiddleware> logger)
        {
            _logger = logger;
        }


        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (CommandException exception)
            {
                await HandleErrorAsync(context, exception);
            }
            catch (QueryException exception)
            {
                await HandleErrorAsync(context, exception);
            }
        }

        private Task HandleErrorAsync(HttpContext context, CqrsException exception)
        {
            _logger.LogError(exception.Message);
            const string reason = "Application error.";
            const HttpStatusCode statusCode = HttpStatusCode.BadRequest;


            var response = new
            {
                reason,
                errors = exception.Errors.Select(item => new
                {
                    error = item
                })
            };

            var payload = JsonConvert.SerializeObject(response);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) statusCode;

            return context.Response.WriteAsync(payload);
        }
    }
}