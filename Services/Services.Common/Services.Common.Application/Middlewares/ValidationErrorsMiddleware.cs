﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Services.Common.Requests.Middlewares
{
    public class ValidationErrorsMiddleware : IMiddleware
    {
        private readonly ILogger<ValidationErrorsMiddleware> _logger;

        public ValidationErrorsMiddleware(ILogger<ValidationErrorsMiddleware> logger)
        {
            _logger = logger;
        }

        private static Task HandleErrorAsync(HttpContext context, ValidationException exception)
        {
            const string reason = "Validation error.";
            const HttpStatusCode statusCode = HttpStatusCode.BadRequest;


            var response = new
            {
                reason,
                items = exception.Errors.Select(item => new
                {
                    name = item.PropertyName,
                    error = item.ErrorMessage
                })
            };

            var payload = JsonConvert.SerializeObject(response);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) statusCode;

            return context.Response.WriteAsync(payload);
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (ValidationException exception)
            {
                _logger.LogError(exception.Message);
                await HandleErrorAsync(context, exception);
            }
        }
    }
}