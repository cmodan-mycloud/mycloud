﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Services.Common.Requests.Exceptions;

namespace Services.Common.Requests.Queries.AuthorizationHeader
{
    public class GetAuthorizationHeaderQuery : IRequest, IRequest<string>
    {
        public GetAuthorizationHeaderQuery(IHttpContextAccessor contextAccessor)
        {
            ContextAccessor = contextAccessor;
        }

        public IHttpContextAccessor ContextAccessor { get; }
    }

    public class GetAuthorizationHeaderQueryHandler : IRequestHandler<GetAuthorizationHeaderQuery, string>
    {
        private readonly ILogger<GetAuthorizationHeaderQueryHandler> _logger;

        public GetAuthorizationHeaderQueryHandler(ILogger<GetAuthorizationHeaderQueryHandler> logger)
        {
            _logger = logger;
        }

        public async Task<string> Handle(GetAuthorizationHeaderQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
                {
                    var headerValue = request.ContextAccessor.HttpContext.Request.Headers[HeaderNames.Authorization]
                        .FirstOrDefault();
                    if (!string.IsNullOrEmpty(headerValue)) return headerValue;

                    _logger.LogError("Unable to find the authorization header on the request!");
                    throw new QueryException("Unable to find the authorization header on the request!");
                },
                cancellationToken
            );
        }
    }
}