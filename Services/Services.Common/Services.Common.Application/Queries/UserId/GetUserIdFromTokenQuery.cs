﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;

namespace Services.Common.Requests.Queries.UserId
{
    public class GetUserIdFromTokenQuery : IRequest<Guid>
    {
        public IEnumerable<Claim> Claims { get; }

        public GetUserIdFromTokenQuery(IEnumerable<Claim> claims)
        {
            Claims = claims;
        }
    }

    public class GetUserIdFromTokenQueryHandler : IRequestHandler<GetUserIdFromTokenQuery, Guid>
    {
        private readonly ILogger<GetUserIdFromTokenQueryHandler> _logger;

        public GetUserIdFromTokenQueryHandler(ILogger<GetUserIdFromTokenQueryHandler> logger)
        {
            _logger = logger;
        }

        public async Task<Guid> Handle(GetUserIdFromTokenQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(
                () =>
                {
                    var claim = request.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);
                    if (claim == null)
                    {
                        _logger.LogError("User id claim was not found in the given token");
                        throw new QueryException("User id claim was not found in the given token");
                    }

                    try
                    {
                        return Guid.Parse(claim.Value);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e.Message);
                        throw new QueryException("Failed to parse the user id from token.");
                    }
                },
                cancellationToken);
        }
    }
}