﻿﻿using System;

namespace Services.Common.Domain.Entities
{
    public class BaseEntity : IEntity
    {
        public Guid Id { get; set; }
    }
}