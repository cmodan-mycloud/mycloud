﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Services.Common.Requests.Interfaces.Utilities;

namespace Services.Common.Infrastructure
{
    public class DateTimeNameCreator : ICreateUniqueName
    {
        public async Task<string> Create(string tail, CancellationToken cancellationToken)
        {
            return await Task.Run(() => DateTime.Now.Ticks + tail, cancellationToken);
        }
    }
}