﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Services.Common.Requests.Interfaces.IO;

namespace Services.Common.Infrastructure.IO
{
    public class ExtensionExtractor : IExtractExtension
    {
        public async Task<string> ExtractExtensionFrom(string fileName, CancellationToken cancellationToken)
        {
            return await Task.Run(() => Path.GetExtension(fileName), cancellationToken);
        }
    }
}