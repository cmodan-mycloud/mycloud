﻿using System.IO;
using Services.Common.Requests.Interfaces.IO;

namespace Services.Common.Infrastructure.IO
{
    public class FileNameExtractor : IHandleFileName
    {
        public string ExtractName(string fileName)
        {
            return Path.GetFileNameWithoutExtension(fileName);
        }
    }
}