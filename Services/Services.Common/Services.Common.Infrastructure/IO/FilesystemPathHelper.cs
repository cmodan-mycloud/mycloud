﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Services.Common.Requests.Interfaces.IO;
using Services.Common.Requests.Interfaces.IO.Models;
using Services.Common.Requests.Interfaces.Utilities;

namespace Services.Common.Infrastructure.IO
{
    public class FilesystemPathHelper : IPathHelper
    {
        private readonly IManipulatePaths _manipulatePaths;
        private readonly string _storageRoot;

        public FilesystemPathHelper(
            IOptions<LocalStoragePath> settings,
            IGuessPlatform guessPlatform,
            IManipulatePaths manipulatePaths
        )
        {
            _manipulatePaths = manipulatePaths;
            var homePath =
                guessPlatform.Guess() == Platform.UnixBased
                    ? Environment.GetEnvironmentVariable(settings.Value.UnixEnv)
                    : Environment.ExpandEnvironmentVariables(settings.Value.WinEnv);

            if (homePath == null)
                throw new ArgumentException(
                    "Null home path detected for uploading. Please check the configuration.");

            _storageRoot = _manipulatePaths
                .Combine(homePath, settings.Value.StorageDirectoryName, CancellationToken.None).Result;
        }

        public async Task<string> GetOnBehalfOf(DirectoryPathModel model, CancellationToken cancellationToken)
        {
            return await _manipulatePaths.Combine(_storageRoot, model.OwnerId.ToString(), cancellationToken);
        }
    }
}