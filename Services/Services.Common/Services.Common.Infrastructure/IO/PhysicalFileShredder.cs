﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Services.Common.Requests.Interfaces.IO;

namespace Services.Common.Infrastructure.IO
{
    public class PhysicalFileShredder : IShredFiles
    {
        public async Task Shred(string directory, string fileName, CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                var path = Path.Combine(directory, fileName);
                var fileInfo = new FileInfo(path);
                fileInfo.Delete();
            }, cancellationToken);
        }
    }
}