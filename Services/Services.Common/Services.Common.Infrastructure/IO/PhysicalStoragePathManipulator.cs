﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Services.Common.Requests.Interfaces.IO;

namespace Services.Common.Infrastructure.IO
{
    public class PhysicalStoragePathManipulator : IManipulatePaths
    {
        public async Task<string> Combine(string first, string second, CancellationToken cancellationToken)
        {
            return await Task.Run(() => Path.Combine(first, second), cancellationToken);
        }

        public async Task<bool> FileExists(string filePath, CancellationToken cancellationToken)
        {
            return await Task.Run(() => File.Exists(filePath), cancellationToken);
        }

        public async Task<bool> DirectoryExists(string directoryPath, CancellationToken cancellationToken)
        {
            return await Task.Run(() => Directory.Exists(directoryPath), cancellationToken);
        }

        public async Task CreateDirectory(string directoryPath, CancellationToken cancellationToken)
        {
            await Task.Run(() => Directory.CreateDirectory(directoryPath), cancellationToken);
        }
    }
}