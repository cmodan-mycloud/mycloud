﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Services.Common.Infrastructure.Persistence
{
    public abstract class BaseDesignTimeFactory
    {
        /// <summary>
        /// The name of the environment on which the app is running (usually prod or development).
        /// </summary>
        private readonly string _environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

        /// <summary>
        /// Connection string name. This should match a connection string from the `appsettings.json` or `appsettings.Development.json`.
        /// </summary>
        protected string ConnectionStringName = "";

        /// <summary>
        /// The name of the project containing the `appsettings` files.
        /// </summary>
        protected string ProjectName = "";

        /// <summary>
        /// Creates the configuration.
        /// </summary>
        /// <returns></returns>
        protected IConfiguration Configuration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), $"../{ProjectName}"))
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{_environmentName}.json", true)
                .Build();
        }
        
    }
}