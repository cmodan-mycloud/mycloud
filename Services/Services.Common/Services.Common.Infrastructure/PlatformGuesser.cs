﻿using System;
using Services.Common.Requests.Interfaces.Utilities;

namespace Services.Common.Infrastructure
{
    public class PlatformGuesser : IGuessPlatform
    {
        public Platform Guess()
        {
            return Environment.OSVersion.Platform == PlatformID.Unix ||
                   Environment.OSVersion.Platform == PlatformID.MacOSX
                ? Platform.UnixBased
                : Platform.Windows;
        }
    }
}