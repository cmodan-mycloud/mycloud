﻿using System.Threading.Tasks;
using RawRabbit;
using Services.Common.Requests.Interfaces.Command;
using Services.Common.Requests.Interfaces.Event;
using Services.Common.Requests.Interfaces.Publisher;

namespace Services.Common.Infrastructure.RabbitMq
{
    public class BusPublisher : IBusPublisher
    {
        private readonly IBusClient _busClient;

        public BusPublisher(IBusClient busClient)
        {
            _busClient = busClient;
        }

        public async Task SendAsync<TCommand>(TCommand command) where TCommand : ICommand
        {
            await _busClient.PublishAsync(command);
        }

        public async Task PublishAsync<TEvent>(TEvent @event) where TEvent : IEvent
        {
            await _busClient.PublishAsync(@event);
        }
    }
}