﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit;
using Services.Common.Requests.Interfaces.Command;
using Services.Common.Requests.Interfaces.Event;
using Services.Common.Requests.Interfaces.Subscriber;

namespace Services.Common.Infrastructure.RabbitMq
{
    public class BusSubscriber : IBusSubscriber
    {
        private readonly IBusClient _busClient;
        private readonly IServiceProvider _serviceProvider;

        public BusSubscriber(IApplicationBuilder app)
        {
            _busClient = app.ApplicationServices.GetService<IBusClient>();
            _serviceProvider = app.ApplicationServices.GetService<IServiceProvider>();
        }

        public IBusSubscriber SubscribeCommand<TCommand>() where TCommand : ICommand
        {
            _busClient.SubscribeAsync<TCommand>(async (command, context) =>
            {
                var handler = _serviceProvider.GetService<ICommandHandler<TCommand>>();
                await handler.HandleAsync(command);
            });
            return this;
        }

        public IBusSubscriber SubscribeEvent<TEvent>() where TEvent : IEvent
        {
            _busClient.SubscribeAsync<TEvent>(async (@event, context) =>
            {
                var handler = _serviceProvider.GetService<IEventHandler<TEvent>>();
                await handler.HandleAsync(@event);
            });
            return this;
        }
    }
}