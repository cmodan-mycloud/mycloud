﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit.vNext;
using Services.Common.Requests.Interfaces.Publisher;
using Services.Common.Requests.Interfaces.Subscriber;

namespace Services.Common.Infrastructure.RabbitMq
{
    public static class RawRabbitExtensions
    {
        public static void AddCustomRawRabbit(this IServiceCollection services)
        {
            // TODO: retrieve rabbit-mq settings from config
            services.AddRawRabbit();
            services.AddTransient<IBusPublisher, BusPublisher>();
        }

        public static IBusSubscriber UseRabbitMqSubscriber(this IApplicationBuilder app)
        {
            return new BusSubscriber(app);
        }
    }
}