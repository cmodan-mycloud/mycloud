﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Services.Common.Requests.Interfaces.Repositories;

namespace Services.Common.Infrastructure.Repositories
{
    public class ReadOnlyRepository<T> : IReadOnlyRepository<T> where T : class
    {
        private readonly DbContext _context;

        public ReadOnlyRepository(DbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<T>> GetAllAsync(
            CancellationToken cancellationToken,
            Func<IQueryable<T>, IQueryable<T>> sharper = null
        )
        {
            var entities = _context.Set<T>().AsNoTracking();
            return sharper != null
                ? await sharper(entities).ToListAsync(cancellationToken)
                : await entities.ToListAsync(cancellationToken);
        }

        public async Task<T> GetByConditionAsync(CancellationToken cancellationToken,
            Expression<Func<T, bool>> expression, Func<IQueryable<T>, IQueryable<T>> sharper = null)
        {
            var entities = _context.Set<T>().Where(expression).AsNoTracking();

            return sharper != null
                ? await sharper(entities).FirstOrDefaultAsync(cancellationToken)
                : await entities.FirstOrDefaultAsync(cancellationToken);
        }

        public async Task<IEnumerable<T>> GetAllByConditionAsync(CancellationToken cancellationToken,
            Expression<Func<T, bool>> expression, Func<IQueryable<T>, IQueryable<T>> sharper = null)
        {
            var entities = _context.Set<T>().Where(expression).AsNoTracking();

            return sharper != null
                ? await sharper(entities).ToListAsync(cancellationToken)
                : await entities.ToListAsync(cancellationToken);
        }
    }
}