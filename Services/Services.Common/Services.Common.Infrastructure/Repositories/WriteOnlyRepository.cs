﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Services.Common.Requests.Interfaces.Repositories;

namespace Services.Common.Infrastructure.Repositories
{
    public class WriteOnlyRepository<T> : IWriteOnlyRepository<T> where T : class
    {
        private readonly DbContext _context;

        public WriteOnlyRepository(DbContext context)
        {
            _context = context;
        }

        public async Task Create(T entity, CancellationToken cancellationToken)
        {
            await _context.Set<T>().AddAsync(entity, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task Update(T entity, CancellationToken cancellationToken)
        {
            _context.Set<T>().Update(entity);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task UpdateRange(IEnumerable<T> entities, CancellationToken cancellationToken)
        {
            _context.Set<T>().UpdateRange(entities);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task Delete(T entity, CancellationToken cancellationToken)
        {
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteRange(IEnumerable<T> entities, CancellationToken cancellationToken)
        {
            _context.Set<T>().RemoveRange(entities);
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}