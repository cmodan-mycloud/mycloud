﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Common.CORS
{
    public static class CorsExtensions
    {
        public static IServiceCollection AddCustomCors(this IServiceCollection services, IConfiguration configuration)
        {
            var origins = configuration.GetSection("cors:hosts")
                .GetChildren()
                .AsEnumerable()
                .Select(x =>
                    x.GetValue<string>("url")
                );

            var policyName = GetCorsPolicyName(configuration);

            services.AddCors(options =>
            {
                options.AddPolicy(policyName, policy =>
                {
                    foreach (var origin in origins)
                        policy
                            .WithOrigins(origin)
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials()
                            .SetIsOriginAllowedToAllowWildcardSubdomains();
                });
            });

            return services;
        }

        private static string GetCorsPolicyName(IConfiguration configuration)
        {
            return configuration.GetSection("cors").GetSection("name").Value;
        }

        public static IApplicationBuilder UseCustomCors(this IApplicationBuilder app, IConfiguration configuration)
        {
            var policyName = GetCorsPolicyName(configuration);
            return app.UseCors(policyName);
        }
    }
}