﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Common.CORS
{
    public static class IdentityProviderExtensions
    {
        public static IServiceCollection AddJwtCustomAuthentication(this IServiceCollection services,
            IConfiguration config)
        {
            var audience = config.GetSection("identity").GetSection("audience").Value;
            var authority = config.GetSection("identity").GetSection("authority").Value;
            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = authority;
                    options.RequireHttpsMetadata = false;
                    options.Audience = audience;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = false
                    };

                    // Sending the access token in the query string is required due to
                    // a limitation in Browser APIs. We restrict it to only calls to the
                    // SignalR hub in this code.
                    // See https://docs.microsoft.com/aspnet/core/signalr/security#access-token-logging
                    // for more information about security considerations when using
                    // the query string to transmit the access token.
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            if (!string.IsNullOrEmpty(context.Token)) return Task.CompletedTask;
                            var accessToken = context.Request.Query["access_token"];
                            context.Token = accessToken;
                            return Task.CompletedTask;
                        }
                    };
                });
            return services;
        }
    }
}