﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Services.Common.Requests.Queries.UserId;
using Services.Download.API.Models;
using Services.Download.Application.Queries.Download;
using Services.Download.Application.Queries.FileName;

namespace Services.Download.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FileController : ControllerBase
    {
        private readonly IMediator _mediator;

        public FileController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Download([FromQuery] DownloadFileRequestModel request,
            [FromHeader] string authorization)
        {
            var userId = await _mediator.Send(new GetUserIdFromTokenQuery(HttpContext.User.Claims));
            var fileName = await _mediator.Send(new GetNameQuery(request.FileId, authorization));
            var result = await _mediator.Send(new DownloadFileQuery(userId, fileName));
            return File(result.Data, result.ContentType);
        }
    }
}