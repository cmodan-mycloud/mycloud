﻿using System;

namespace Services.Download.API.Models
{
    public class DownloadFileRequestModel
    {
        public Guid FileId { get; set; }
    }
}