﻿using System;

namespace Services.Download.API.Models
{
    public class DownloadFolderRequestModel
    {
        public Guid FolderId { get; set; }
    }
}