﻿using System;

namespace Services.Download.API.Models
{
    public class DownloadPhotoRequestModel
    {
        public Guid PhotoId { get; set; }
    }
}