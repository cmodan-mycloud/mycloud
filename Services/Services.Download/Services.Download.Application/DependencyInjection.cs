﻿using System;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Services.Common.Infrastructure;
using Services.Common.Infrastructure.IO;
using Services.Common.Requests.Behaviors.Validation;
using Services.Common.Requests.Interfaces.IO;
using Services.Common.Requests.Interfaces.Utilities;
using Services.Common.Requests.Middlewares;
using Services.Common.Requests.Queries.UserId;
using Services.Download.Application.Download;
using Services.Download.Application.Queries.Download;
using Services.Download.Common.Download;
using Services.Download.Common.MiMeHelper;

namespace Services.Download.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddMediatR(typeof(DownloadFileQuery));
            services.AddTransient<IRequestHandler<GetUserIdFromTokenQuery, Guid>, GetUserIdFromTokenQueryHandler>();
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            services.AddHttpClient();

            services.AddTransient<IPathHelper, FilesystemPathHelper>();
            services.AddTransient<IDownloadManager, LocalStorageDownloadManager>();
            services.AddSingleton<IGuessMiMeType, MiMeTypeGuesser>();
            services.AddSingleton<IGuessPlatform, PlatformGuesser>();
            services.AddTransient<IManipulatePaths, PhysicalStoragePathManipulator>();

            services.AddTransient<ValidationErrorsMiddleware>();
            services.AddTransient<QueryCommandExceptionMiddleware>();

            return services;
        }
    }
}