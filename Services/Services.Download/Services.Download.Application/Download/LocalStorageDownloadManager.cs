﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Services.Download.Common.Download;

namespace Services.Download.Application.Download
{
    public class LocalStorageDownloadManager : IDownloadManager
    {
        public async Task<byte[]> Download(DownloadModel model, CancellationToken cancellationToken)
        {
            var fileAbsolutePath = Path.Combine(model.Directory, model.FileName);
            byte[] result;

            await using (var stream = File.Open(fileAbsolutePath, FileMode.Open))
            {
                result = new byte[stream.Length];
                await stream.ReadAsync(result, 0, (int) stream.Length, cancellationToken);
            }

            return result;
        }
    }
}