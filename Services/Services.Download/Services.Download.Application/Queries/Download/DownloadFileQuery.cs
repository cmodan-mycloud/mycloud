﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.IO;
using Services.Common.Requests.Interfaces.IO.Models;
using Services.Download.Common.Download;
using Services.Download.Common.MiMeHelper;

namespace Services.Download.Application.Queries.Download
{
    public class DownloadFileQuery : IRequest<DownloadResult>
    {
        public DownloadFileQuery(Guid ownerId, string fileName)
        {
            OwnerId = ownerId;
            FileName = fileName;
        }

        public Guid OwnerId { get; }
        public string FileName { get; }
    }

    public class DownloadFileQueryHandler : IRequestHandler<DownloadFileQuery, DownloadResult>
    {
        private readonly IDownloadManager _downloadManager;
        private readonly IGuessMiMeType _guessMiMeType;
        private readonly IPathHelper _pathHelper;

        public DownloadFileQueryHandler(
            IDownloadManager downloadManager,
            IGuessMiMeType guessMiMeType,
            IPathHelper pathHelper
        )
        {
            _downloadManager = downloadManager;
            _guessMiMeType = guessMiMeType;
            _pathHelper = pathHelper;
        }

        public async Task<DownloadResult> Handle(DownloadFileQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var directory =
                    await _pathHelper.GetOnBehalfOf(new DirectoryPathModel(request.OwnerId), cancellationToken);
                var data = await _downloadManager.Download(new DownloadModel(directory, request.FileName),
                    cancellationToken);
                var contentType = _guessMiMeType.Guess(request.FileName);
                return new DownloadResult(data, contentType);
            }
            catch (Exception e)
            {
                throw new QueryException($"Failed to download file {request.FileName}", e);
            }
        }
    }

    public class DownloadResult
    {
        public DownloadResult(byte[] data, string contentType)
        {
            Data = data;
            ContentType = contentType;
        }

        public byte[] Data { get; }
        public string ContentType { get; }
    }
}