﻿using FluentValidation;

namespace Services.Download.Application.Queries.Download
{
    public class DownloadFileQueryValidator : AbstractValidator<DownloadFileQuery>
    {
        public DownloadFileQueryValidator()
        {
            RuleFor(p => p.FileName).NotEmpty();
            RuleFor(p => p.OwnerId).NotEmpty();
        }
    }
}