﻿using FluentValidation;

namespace Services.Download.Application.Queries.FileName
{
    public class GetFileMetaDataQueryQueryValidator : AbstractValidator<GetNameQuery>
    {
        public GetFileMetaDataQueryQueryValidator()
        {
            RuleFor(p => p.FileId).NotEmpty();
        }
    }
}