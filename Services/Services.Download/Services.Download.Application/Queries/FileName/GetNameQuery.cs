﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Services.Common.Requests.Exceptions;

namespace Services.Download.Application.Queries.FileName
{
    public class GetNameQuery : IRequest<string>
    {
        public GetNameQuery(Guid fileId, string bearerString)
        {
            FileId = fileId;
            BearerString = bearerString;
        }

        public Guid FileId { get; }
        public string BearerString { get; }
    }

    public class GetNameQueryHandler : IRequestHandler<GetNameQuery, string>
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IConfiguration _configuration;

        public GetNameQueryHandler(IHttpClientFactory clientFactory, IConfiguration configuration)
        {
            _clientFactory = clientFactory;
            _configuration = configuration;
        }

        public async Task<string> Handle(GetNameQuery request, CancellationToken cancellationToken)
        {
            var fileMetaDataEndpointUrl = _configuration.GetSection("fileMetaData:fileNameUrl").Value;
            var nameRequest = new HttpRequestMessage(
                HttpMethod.Get,
                fileMetaDataEndpointUrl
            );
            var accessToken = request.BearerString.Split(" ")[1];
            nameRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var payload = new ByteArrayContent(Encoding.UTF8.GetBytes(request.FileId.ToString()));
            nameRequest.Content = new MultipartFormDataContent {{payload, "FileId"}};

            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(nameRequest, cancellationToken);

            if (!response.IsSuccessStatusCode) throw new QueryException("Failed to retrieve file meta-data.");

            var content = await response.Content.ReadAsStringAsync();
            var file = JsonConvert.DeserializeObject<FileNameResponse>(content);

            return file.fileName;
        }
    }

    public class FileNameResponse
    {
        public string fileName { get; set; }
    }
}