﻿namespace Services.Download.Common.Download
{
    public class DownloadModel
    {
        public DownloadModel(string directory, string fileName)
        {
            FileName = fileName;
            Directory = directory;
        }

        public string Directory { get; }
        public string FileName { get; }
    }
}