﻿using System.Threading;
using System.Threading.Tasks;

namespace Services.Download.Common.Download
{
    public interface IDownloadManager
    {
        public Task<byte[]> Download(DownloadModel model, CancellationToken cancellationToken);
    }
}