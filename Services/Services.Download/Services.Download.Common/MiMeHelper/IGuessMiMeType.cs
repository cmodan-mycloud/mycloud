﻿namespace Services.Download.Common.MiMeHelper
{
    public interface IGuessMiMeType
    {
        public string Guess(string fileName);
    }
}