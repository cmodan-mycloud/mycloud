﻿using Microsoft.AspNetCore.StaticFiles;

namespace Services.Download.Common.MiMeHelper
{
    public class MiMeTypeGuesser : IGuessMiMeType
    {
        public string Guess(string fileName)
        {
            // TODO: enrich with additional mappings
            var guesser = new FileExtensionContentTypeProvider();
            return guesser.TryGetContentType(fileName, out var contentType) ? contentType : "application/octet-stream";
        }
    }
}