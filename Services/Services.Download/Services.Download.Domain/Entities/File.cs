﻿using System;

namespace Services.Download.Domain.Entities
{
    public class File
    {
        public File(Guid ownerId, string fileName, string newFileName, string extension, long sizeInBytes)
        {
            OwnerId = ownerId;
            FileName = fileName;
            NewFileName = newFileName;
            Extension = extension;
            SizeInBytes = sizeInBytes;
        }

        public Guid OwnerId { get; set; }
        public string FileName { get; set; }
        public string NewFileName { get; set; }
        public string Extension { get; set; }
        public long SizeInBytes { get; set; }
    }
}