﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Common.Requests.Queries.UserId;
using Services.FileMetaData.API.Models.Request.File;
using Services.FileMetaData.API.Models.Response;
using Services.FileMetaData.Application.Commands.Files.Delete;
using Services.FileMetaData.Application.Commands.Files.Update;
using Services.FileMetaData.Application.Queries.Files.ById;
using Services.FileMetaData.Application.Queries.Files.Name;

namespace Services.FileMetaData.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class FileController : Controller
    {
        private readonly IMediator _mediator;

        public FileController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Name([FromForm] FileNameRequest request)
        {
            var userId = await _mediator.Send(new GetUserIdFromTokenQuery(HttpContext.User.Claims));
            var fileName = await _mediator.Send(new GetFileNameByIdQuery(request.FileId, userId));
            return Ok(new FileNameResponse(fileName));
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] DeleteFileRequest request)
        {
            var userId = await _mediator.Send(new GetUserIdFromTokenQuery(HttpContext.User.Claims));
            var file = await _mediator.Send(new GetFileByIdQuery(request.FileId, userId));
            await _mediator.Send(new DeleteFileCommand(file, userId));
            return Ok();
        }

        [HttpPatch]
        public async Task<IActionResult> Update([FromForm] UpdateFileNameRequest request)
        {
            var userId = await _mediator.Send(new GetUserIdFromTokenQuery(HttpContext.User.Claims));
            var file = await _mediator.Send(new GetFileByIdQuery(request.FileId, userId));
            await _mediator.Send(new UpdateFileNameCommand(request.Name, file));
            return Ok();
        }
    }
}