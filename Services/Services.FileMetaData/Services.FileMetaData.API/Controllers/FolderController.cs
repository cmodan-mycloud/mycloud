﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Common.Requests.Queries.UserId;
using Services.FileMetaData.API.Models.Request.Folder;
using Services.FileMetaData.API.Models.Response;
using Services.FileMetaData.Application.Commands.Folder.Create;
using Services.FileMetaData.Application.Commands.Folder.Delete;
using Services.FileMetaData.Application.Commands.Folder.UpdateName;
using Services.FileMetaData.Application.Queries.Files.ByFolder;
using Services.FileMetaData.Application.Queries.Folders.GetById;
using Services.FileMetaData.Application.Queries.Folders.RootId;

namespace Services.FileMetaData.API.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class FolderController : Controller
    {
        private readonly IMediator _mediator;

        public FolderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("items")]
        public async Task<IActionResult> Items([FromQuery] FolderItems request)
        {
            var userId = await _mediator.Send(new GetUserIdFromTokenQuery(HttpContext.User.Claims));
            var folderItems = await _mediator.Send(new GetFolderItemsQuery(request.FolderId, userId));
            return Ok(folderItems);
        }

        [HttpGet("root")]
        public async Task<IActionResult> Root()
        {
            var userId = await _mediator.Send(new GetUserIdFromTokenQuery(HttpContext.User.Claims));
            var rootFolderId = await _mediator.Send(new GetRootFolderIdQuery(userId));
            var folderItems = await _mediator.Send(new GetFolderItemsQuery(rootFolderId, userId));
            return Ok(folderItems);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromForm] CreateFolderRequestModel request)
        {
            var userId = await _mediator.Send(new GetUserIdFromTokenQuery(HttpContext.User.Claims));
            var parentFolder = await _mediator.Send(new GetFolderByIdQuery(request.ParentId, userId));
            var folderId =
                await _mediator.Send(new CreateFolderUnderParentCommand(userId, request.Name, parentFolder));
            return Ok(new FolderCreatedResponse(folderId));
        }

        [HttpDelete("delete")]
        public async Task<IActionResult> Delete([FromQuery] DeleteFolderRequestModel request)
        {
            var userId = await _mediator.Send(new GetUserIdFromTokenQuery(HttpContext.User.Claims));
            var folder = await _mediator.Send(new GetFolderByIdQuery(request.FolderId, userId));
            await _mediator.Send(new DeleteFolderCommand(userId, folder));
            return Accepted();
        }

        [HttpPatch("update")]
        public async Task<IActionResult> UpdateFolderName([FromForm] UpdateFolderNameRequestModel request)
        {
            var userId = await _mediator.Send(new GetUserIdFromTokenQuery(HttpContext.User.Claims));
            var folder = await _mediator.Send(new GetFolderByIdQuery(request.FolderId, userId));
            await _mediator.Send(new UpdateFolderNameCommand(request.Name, folder));
            return Accepted();
        }
    }
}