﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Common.Requests.Queries.UserId;
using Services.FileMetaData.Application.Queries.Files.PhotosOnly;

namespace Services.FileMetaData.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class PhotosController : Controller
    {
        private readonly IMediator _mediator;

        public PhotosController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Photos()
        {
            var userId = await _mediator.Send(new GetUserIdFromTokenQuery(HttpContext.User.Claims));
            var folderItems = await _mediator.Send(new GetPhotosOnlyQuery(userId));
            return Ok(folderItems);
        }
    }
}