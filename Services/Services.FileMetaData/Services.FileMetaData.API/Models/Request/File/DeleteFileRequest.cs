﻿using System;

namespace Services.FileMetaData.API.Models.Request.File
{
    public class DeleteFileRequest
    {
        public Guid FileId { get; set; }
    }
}