﻿using System;

namespace Services.FileMetaData.API.Models.Request.File
{
    public class FileDetailsRequest
    {
        public Guid FileId { get; set; }
    }
}