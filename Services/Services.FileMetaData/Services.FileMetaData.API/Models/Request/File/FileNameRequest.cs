﻿using System;

namespace Services.FileMetaData.API.Models.Request.File
{
    public class FileNameRequest
    {
        public Guid FileId { get; set; }
    }
}