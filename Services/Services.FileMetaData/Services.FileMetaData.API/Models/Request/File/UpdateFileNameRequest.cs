﻿using System;

namespace Services.FileMetaData.API.Models.Request.File
{
    public class UpdateFileNameRequest
    {
        public Guid FileId { get; set; }
        public string Name { get; set; }
    }
}