﻿using System;

namespace Services.FileMetaData.API.Models.Request.Folder
{
    public class CreateFolderRequestModel
    {
        public string Name { get; set; }
        public Guid ParentId { get; set; }
    }
}