﻿using System;

namespace Services.FileMetaData.API.Models.Request.Folder
{
    public class DeleteFolderRequestModel
    {
        public Guid FolderId { get; set; }
    }
}