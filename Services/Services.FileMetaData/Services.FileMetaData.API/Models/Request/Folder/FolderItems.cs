﻿using System;

namespace Services.FileMetaData.API.Models.Request.Folder
{
    public class FolderItems
    {
        public Guid FolderId { get; set; }
    }
}