﻿using System;

namespace Services.FileMetaData.API.Models.Request.Folder
{
    public class UpdateFolderNameRequestModel
    {
        public string Name { get; set; }
        public Guid FolderId { get; set; }
    }
}