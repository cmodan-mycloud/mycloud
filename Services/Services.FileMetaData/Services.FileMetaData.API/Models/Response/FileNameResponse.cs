﻿using Newtonsoft.Json;

namespace Services.FileMetaData.API.Models.Response
{
    public class FileNameResponse
    {
        [JsonConstructor]
        public FileNameResponse(string fileName)
        {
            FileName = fileName;
        }

        public string FileName { get; }
    }
}