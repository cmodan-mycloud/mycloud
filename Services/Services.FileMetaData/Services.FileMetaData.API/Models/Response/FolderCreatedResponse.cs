﻿using System;
using Newtonsoft.Json;

namespace Services.FileMetaData.API.Models.Response
{
    public class FolderCreatedResponse
    {
        public Guid FolderId { get; set; }

        [JsonConstructor]
        public FolderCreatedResponse(Guid folderId)
        {
            FolderId = folderId;
        }
    }
}