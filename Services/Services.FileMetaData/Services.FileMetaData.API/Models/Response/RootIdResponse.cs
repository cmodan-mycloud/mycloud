﻿using System;
using Newtonsoft.Json;

namespace Services.FileMetaData.API.Models.Response
{
    public class RootIdResponse
    {
        public Guid Root { get; set; }

        [JsonConstructor]
        public RootIdResponse(Guid root)
        {
            Root = root;
        }
    }
}