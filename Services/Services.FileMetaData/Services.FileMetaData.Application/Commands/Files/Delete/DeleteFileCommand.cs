﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Application.Events.Files;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Application.Commands.Files.Delete
{
    public class DeleteFileCommand : IRequest
    {
        public DeleteFileCommand(File file, Guid ownerId)
        {
            File = file;
            OwnerId = ownerId;
        }

        public File File { get; }
        public Guid OwnerId { get; }
    }

    public class DeleteFileCommandHandler : IRequestHandler<DeleteFileCommand, Unit>
    {
        private readonly IWriteOnlyRepository<File> _fileRepository;
        private readonly ILogger<DeleteFileCommandHandler> _logger;
        private readonly IMediator _mediator;

        public DeleteFileCommandHandler(IWriteOnlyRepository<File> fileRepository,
            ILogger<DeleteFileCommandHandler> logger, IMediator mediator)
        {
            _fileRepository = fileRepository;
            _logger = logger;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(DeleteFileCommand request, CancellationToken cancellationToken)
        {
            try
            {
                await _fileRepository.Delete(request.File, cancellationToken);
            }
            catch (Exception e)
            {
                throw new CommandException($"Failed to delete file {request.File.Id}", e);
            }

            _logger.LogInformation($"Successfully deleted file {request.File.Id}!");
            await _mediator.Publish(new FileDeletedEvent(request.OwnerId, request.File.NewFileName), cancellationToken);
            return Unit.Value;
        }
    }
}