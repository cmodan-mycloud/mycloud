﻿using FluentValidation;

namespace Services.FileMetaData.Application.Commands.Files.Delete
{
    public class DeleteFileCommandValidator : AbstractValidator<DeleteFileCommand>
    {
        public DeleteFileCommandValidator()
        {
            RuleFor(p => p.File).NotEmpty();
            RuleFor(p => p.OwnerId).NotEmpty();
        }
    }
}