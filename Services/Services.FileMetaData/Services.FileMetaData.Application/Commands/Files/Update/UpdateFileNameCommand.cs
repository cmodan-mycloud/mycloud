﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Application.Commands.Files.Update
{
    public class UpdateFileNameCommand : IRequest
    {
        public string Name { get; }

        public File File { get; }

        public UpdateFileNameCommand(string name, File file)
        {
            Name = name;
            File = file;
        }
    }

    public class UpdateFileNameCommandHandler : IRequestHandler<UpdateFileNameCommand, Unit>
    {
        private readonly IWriteOnlyRepository<File> _writeOnlyRepository;
        private readonly ILogger<UpdateFileNameCommandHandler> _logger;

        public UpdateFileNameCommandHandler(IWriteOnlyRepository<File> writeOnlyRepository,
            ILogger<UpdateFileNameCommandHandler> logger)
        {
            _writeOnlyRepository = writeOnlyRepository;
            _logger = logger;
        }

        public async Task<Unit> Handle(UpdateFileNameCommand request, CancellationToken cancellationToken)
        {
            try
            {
                request.File.FileName = request.Name;
                await _writeOnlyRepository.Update(request.File, cancellationToken);
                _logger.LogInformation(
                    $"Successfully updated folder {request.File.Id} on behalf of user {request.File.OwnerId}.");
            }
            catch (Exception e)
            {
                throw new CommandException(
                    $"Failed to update folder {request.File.Id} on behalf of user {request.File.OwnerId}.", e);
            }

            return Unit.Value;
        }
    }
}