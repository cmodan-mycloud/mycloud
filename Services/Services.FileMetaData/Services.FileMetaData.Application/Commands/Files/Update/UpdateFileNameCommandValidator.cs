﻿using FluentValidation;

namespace Services.FileMetaData.Application.Commands.Files.Update
{
    public class UpdateFileNameCommandValidator: AbstractValidator<UpdateFileNameCommand>
    {
        public UpdateFileNameCommandValidator()
        {
            RuleFor(p => p.File).NotEmpty();
            RuleFor(p => p.Name).NotEmpty();
        }
    }
}