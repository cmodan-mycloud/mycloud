﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;

namespace Services.FileMetaData.Application.Commands.Folder.Create
{
    public class CreateFolderUnderParentCommand : IRequest<Guid>
    {
        public CreateFolderUnderParentCommand(Guid ownerId, string name, Domain.Entities.Folder parent)
        {
            OwnerId = ownerId;
            Name = name;
            Parent = parent;
        }

        public Guid OwnerId { get; }
        public string Name { get; }
        public Domain.Entities.Folder Parent { get; }
    }

    public class CreateFolderUnderParentCommandHandler : IRequestHandler<CreateFolderUnderParentCommand, Guid>
    {
        private readonly ILogger<CreateFolderUnderParentCommandHandler> _logger;
        private readonly IWriteOnlyRepository<Domain.Entities.Folder> _writeOnlyRepository;

        public CreateFolderUnderParentCommandHandler(IWriteOnlyRepository<Domain.Entities.Folder> writeOnlyRepository,
            ILogger<CreateFolderUnderParentCommandHandler> logger)
        {
            _writeOnlyRepository = writeOnlyRepository;
            _logger = logger;
        }

        public async Task<Guid> Handle(CreateFolderUnderParentCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var id = Guid.NewGuid();
                await _writeOnlyRepository.Create(new Domain.Entities.Folder
                {
                    Id = id,
                    Name = request.Name,
                    OwnerId = request.OwnerId,
                    ParentId = request.Parent.Id,
                    CreatedOn = DateTime.Now
                }, cancellationToken);
                _logger.LogInformation(
                    $"Successfully created folder {request.Name} on behalf of user {request.OwnerId}.");
                return id;
            }
            catch (Exception e)
            {
                throw new CommandException($"Failed to create folder {request.Name}.", e);
            }
        }
    }
}