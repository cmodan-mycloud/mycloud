﻿using FluentValidation;

namespace Services.FileMetaData.Application.Commands.Folder.Create
{
    public class CreateFolderUnderParentCommandValidator : AbstractValidator<CreateFolderUnderParentCommand>
    {
        public CreateFolderUnderParentCommandValidator()
        {
            RuleFor(p => p.Name).NotEmpty();
            RuleFor(p => p.Parent).NotEmpty();
            RuleFor(p => p.OwnerId).NotEmpty();
        }
    }
}