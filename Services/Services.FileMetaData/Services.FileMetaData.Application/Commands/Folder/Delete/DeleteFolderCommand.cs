﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;

namespace Services.FileMetaData.Application.Commands.Folder.Delete
{
    public class DeleteFolderCommand : IRequest
    {
        public DeleteFolderCommand(Guid ownerId, Domain.Entities.Folder folder)
        {
            OwnerId = ownerId;
            Folder = folder;
        }

        public Guid OwnerId { get; }
        public Domain.Entities.Folder Folder { get; }
    }

    public class DeleteFolderCommandHandler : IRequestHandler<DeleteFolderCommand, Unit>
    {
        private readonly ILogger<DeleteFolderCommandHandler> _logger;
        private readonly IWriteOnlyRepository<Domain.Entities.Folder> _writeOnlyRepository;

        public DeleteFolderCommandHandler(IWriteOnlyRepository<Domain.Entities.Folder> writeOnlyRepository,
            ILogger<DeleteFolderCommandHandler> logger)
        {
            _writeOnlyRepository = writeOnlyRepository;
            _logger = logger;
        }

        public async Task<Unit> Handle(DeleteFolderCommand request, CancellationToken cancellationToken)
        {
            try
            {
                await _writeOnlyRepository.Delete(request.Folder, cancellationToken);
                _logger.LogInformation(
                    $"Successfully deleted folder {request.Folder.Name} on behalf of {request.OwnerId}");
                // TODO: post event and delete children folders and directories
            }
            catch (Exception e)
            {
                throw new CommandException($"Failed to delete folder {request.Folder.Name}.", e);
            }

            return Unit.Value;
        }
    }
}