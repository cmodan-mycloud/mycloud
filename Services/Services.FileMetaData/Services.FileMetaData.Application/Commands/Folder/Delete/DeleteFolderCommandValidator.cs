﻿using FluentValidation;

namespace Services.FileMetaData.Application.Commands.Folder.Delete
{
    public class DeleteFolderCommandValidator : AbstractValidator<DeleteFolderCommand>
    {
        public DeleteFolderCommandValidator()
        {
            RuleFor(p => p.Folder).NotEmpty();
            RuleFor(p => p.OwnerId).NotEmpty();
        }
    }
}