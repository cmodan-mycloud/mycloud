﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;

namespace Services.FileMetaData.Application.Commands.Folder.UpdateName
{
    public class UpdateFolderNameCommand : IRequest
    {
        public UpdateFolderNameCommand(string name, Domain.Entities.Folder folder)
        {
            Name = name;
            Folder = folder;
        }

        public string Name { get; }
        public Domain.Entities.Folder Folder { get; }
    }

    public class UpdateFolderNameCommandHandler : IRequestHandler<UpdateFolderNameCommand, Unit>
    {
        private readonly ILogger<UpdateFolderNameCommandHandler> _logger;
        private readonly IWriteOnlyRepository<Domain.Entities.Folder> _writeOnlyRepository;

        public UpdateFolderNameCommandHandler(IWriteOnlyRepository<Domain.Entities.Folder> writeOnlyRepository,
            ILogger<UpdateFolderNameCommandHandler> logger)
        {
            _writeOnlyRepository = writeOnlyRepository;
            _logger = logger;
        }

        public async Task<Unit> Handle(UpdateFolderNameCommand request, CancellationToken cancellationToken)
        {
            try
            {
                request.Folder.Name = request.Name;
                await _writeOnlyRepository.Update(request.Folder, cancellationToken);
                _logger.LogInformation(
                    $"Successfully updated folder name {request.Folder.Name} to {request.Name}.");
            }
            catch (Exception e)
            {
                throw new CommandException($"Failed to update folder name {request.Folder.Name} to {request.Name}.", e);
            }

            return Unit.Value;
        }
    }
}