﻿using FluentValidation;

namespace Services.FileMetaData.Application.Commands.Folder.UpdateName
{
    public class UpdateFolderNameCommandValidator : AbstractValidator<UpdateFolderNameCommand>
    {
        public UpdateFolderNameCommandValidator()
        {
            RuleFor(p => p.Name).NotEmpty();
        }
    }
}