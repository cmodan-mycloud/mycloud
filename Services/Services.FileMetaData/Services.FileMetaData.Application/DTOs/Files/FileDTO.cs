﻿using System;

namespace Services.FileMetaData.Application.DTOs.Files
{
    public class FileDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public long SizeInBytes { get; set; }
        public string Extension { get; set; }
        public FolderDto FolderDto { get; set; }
    }
}