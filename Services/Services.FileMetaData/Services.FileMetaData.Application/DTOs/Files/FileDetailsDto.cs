﻿using System;

namespace Services.FileMetaData.Application.DTOs.Files
{
    public class FileDetailsDto
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public long SizeInBytes { get; set; }

        public Guid FolderId { get; set; }
    }
}