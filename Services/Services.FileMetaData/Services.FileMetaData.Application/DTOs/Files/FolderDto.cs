﻿using System;

namespace Services.FileMetaData.Application.DTOs.Files
{
    public class FolderDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid ParentId { get; set; }
    }
}