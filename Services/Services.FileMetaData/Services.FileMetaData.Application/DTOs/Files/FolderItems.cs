﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Services.FileMetaData.Application.DTOs.Files
{
    public class FolderItems
    {
        [JsonConstructor]
        public FolderItems(IEnumerable<FileDto> files, IEnumerable<FolderDto> folders, FolderDto currentFolder)
        {
            Files = files;
            Folders = folders;
            CurrentFolder = currentFolder;
        }

        public FolderDto CurrentFolder { get; set; }
        public IEnumerable<FileDto> Files { get; }
        public IEnumerable<FolderDto> Folders { get; }
    }
}