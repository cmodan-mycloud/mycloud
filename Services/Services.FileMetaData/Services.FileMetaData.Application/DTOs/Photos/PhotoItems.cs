﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Services.FileMetaData.Application.DTOs.Files;

namespace Services.FileMetaData.Application.DTOs.Photos
{
    public class PhotoItems
    {
        [JsonConstructor]
        public PhotoItems(IEnumerable<FileDto> photos)
        {
            Photos = photos;
        }

        public IEnumerable<FileDto> Photos { get; }
    }
}