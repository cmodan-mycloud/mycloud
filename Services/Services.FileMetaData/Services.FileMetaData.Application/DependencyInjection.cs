﻿using System;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Services.Common.Infrastructure;
using Services.Common.Infrastructure.IO;
using Services.Common.Infrastructure.RabbitMq;
using Services.Common.Requests.Behaviors.Validation;
using Services.Common.Requests.Events;
using Services.Common.Requests.Interfaces.Event;
using Services.Common.Requests.Interfaces.IO;
using Services.Common.Requests.Interfaces.Utilities;
using Services.Common.Requests.Mappings;
using Services.Common.Requests.Middlewares;
using Services.Common.Requests.Queries.UserId;
using Services.FileMetaData.Application.Handlers;
using Services.FileMetaData.Application.Queries.Files.ByFolder;

namespace Services.FileMetaData.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddMediatR(typeof(GetFolderItemsQuery));

            // custom handlers
            services.AddTransient<IRequestHandler<GetUserIdFromTokenQuery, Guid>, GetUserIdFromTokenQueryHandler>();

            // pipeline behavior
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));


            // provide event handlers 
            services.AddTransient<IEventHandler<FileUploadedEvent>, FileUploadedEventHandler>();
            services.AddTransient<IEventHandler<UserRegisteredEvent>, UserRegisteredEventHandler>();
            // use BUS client
            services.AddCustomRawRabbit();
            // auto-mapping
            services.AddAutoMapper(typeof(MappingProfile));
            // misc
            services.AddTransient<IPathHelper, FilesystemPathHelper>();
            services.AddTransient<IGuessPlatform, PlatformGuesser>();
            services.AddTransient<IManipulatePaths, PhysicalStoragePathManipulator>();
            services.AddTransient<IShredFiles, PhysicalFileShredder>();

            services.AddTransient<ValidationErrorsMiddleware>();
            services.AddTransient<QueryCommandExceptionMiddleware>();

            return services;
        }
    }
}