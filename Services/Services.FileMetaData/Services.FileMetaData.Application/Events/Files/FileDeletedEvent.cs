﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Interfaces.IO;
using Services.Common.Requests.Interfaces.IO.Models;

namespace Services.FileMetaData.Application.Events.Files
{
    public class FileDeletedEvent : INotification
    {
        public FileDeletedEvent(Guid ownerId, string diskName)
        {
            OwnerId = ownerId;
            DiskName = diskName;
        }

        public Guid OwnerId { get; }
        public string DiskName { get; }
    }

    public class FileDeletedEventHandler : INotificationHandler<FileDeletedEvent>
    {
        private readonly ILogger<FileDeletedEventHandler> _logger;
        private readonly IPathHelper _pathHelper;
        private readonly IShredFiles _shredder;

        public FileDeletedEventHandler(
            IPathHelper pathHelper,
            ILogger<FileDeletedEventHandler> logger,
            IShredFiles shredder
        )
        {
            _pathHelper = pathHelper;
            _logger = logger;
            _shredder = shredder;
        }

        public async Task Handle(FileDeletedEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                var directory =
                    await _pathHelper.GetOnBehalfOf(new DirectoryPathModel(notification.OwnerId), cancellationToken);
                await _shredder.Shred(directory, notification.DiskName, cancellationToken);
                _logger.LogInformation(
                    $"Successfully removed the physical entry for photo {notification.DiskName} on behalf of user {notification.OwnerId}.");
            }
            catch (Exception e)
            {
                _logger.LogError(
                    $"Failed to remove the physical entry for photo {notification.DiskName} on behalf of user {notification.OwnerId}. Reason: {e.Message}.");
                // TODO: publish failure event
            }
        }
    }
}