﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Services.Common.Requests.DTOs;
using Services.Common.Requests.Events;
using Services.Common.Requests.Interfaces.Publisher;

namespace Services.FileMetaData.Application.Events.Files
{
    public class FileSavedEvent : INotification
    {
        public FileSavedEvent(
            Guid id,
            string extension,
            string fileName,
            FolderDto folder,
            long sizeInBytes,
            Guid ownerId,
            DateTime createdOn)
        {
            Id = id;
            Extension = extension;
            FileName = fileName;
            Folder = folder;
            SizeInBytes = sizeInBytes;
            OwnerId = ownerId;
            CreatedOn = createdOn;
        }

        public Guid Id { get; }
        public string Extension { get; }
        public string FileName { get; }
        public FolderDto Folder { get; }
        public long SizeInBytes { get; }
        public Guid OwnerId { get; }
        public DateTime CreatedOn { get; }
    }

    public class FileSavedEventHandler : INotificationHandler<FileSavedEvent>
    {
        private readonly IBusPublisher _busPublisher;

        public FileSavedEventHandler(IBusPublisher busPublisher)
        {
            _busPublisher = busPublisher;
        }

        public async Task Handle(FileSavedEvent notification, CancellationToken cancellationToken)
        {
            await _busPublisher.PublishAsync(new FileMetaDataSavedEvent(
                notification.Id,
                notification.Extension,
                notification.FileName,
                notification.Folder,
                notification.SizeInBytes,
                notification.OwnerId,
                notification.CreatedOn
            ));
        }
    }
}