﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.DTOs;
using Services.Common.Requests.Events;
using Services.Common.Requests.Interfaces.Event;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Application.Events.Files;
using Services.FileMetaData.Application.Queries.Folders.RootId;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Application.Handlers
{
    public class FileUploadedEventHandler : IEventHandler<FileUploadedEvent>
    {
        private readonly ILogger<FileUploadedEventHandler> _logger;
        private readonly IMediator _mediator;
        private readonly IReadOnlyRepository<Folder> _readOnlyFolderRepository;
        private readonly IWriteOnlyRepository<File> _writeOnlyFileRepository;

        public FileUploadedEventHandler(
            IWriteOnlyRepository<File> writeOnlyFileRepository,
            ILogger<FileUploadedEventHandler> logger,
            IReadOnlyRepository<Folder> readOnlyFolderRepository,
            IMediator mediator
        )
        {
            _writeOnlyFileRepository = writeOnlyFileRepository;
            _logger = logger;
            _readOnlyFolderRepository = readOnlyFolderRepository;
            _mediator = mediator;
        }

        public async Task HandleAsync(FileUploadedEvent @event)
        {
            try
            {
                var folderId = @event.FolderId;
                if (folderId == Guid.Empty)
                {
                    _logger.LogWarning("No folder was provided, will default to root!");
                    folderId = await _mediator.Send(new GetRootFolderIdQuery(@event.OwnerId));
                }

                var folder = await _readOnlyFolderRepository.GetByConditionAsync(
                    CancellationToken.None,
                    f => f.Id == folderId && f.OwnerId == @event.OwnerId
                );

                if (folder == null)
                {
                    _logger.LogInformation($"No folder found for ID {folderId}!");
                    throw new InvalidEnumArgumentException($"Folder {folderId} was not found!");
                }

                var file = new File
                {
                    Extension = @event.Extension,
                    FileName = @event.FileName,
                    FolderId = folder.Id,
                    NewFileName = @event.NewFileName,
                    SizeInBytes = @event.SizeInBytes,
                    OwnerId = @event.OwnerId,
                    CreatedOn = DateTime.Now
                };

                await _writeOnlyFileRepository.Create(file, CancellationToken.None);
                _logger.LogInformation($@"Successfully saved {@event.FileName} in {folderId}");

                var folderDto = new FolderDto(
                    folder.Id,
                    folder.Name,
                    folder.OwnerId,
                    folder.ParentId,
                    folder.CreatedOn
                );

                await _mediator.Publish(new FileSavedEvent(
                    file.Id,
                    file.Extension,
                    file.FileName,
                    folderDto,
                    file.SizeInBytes,
                    file.OwnerId,
                    file.CreatedOn
                ));
            }
            catch (Exception e)
            {
                _logger.LogError($"Failed to create file metadata. Reason: {e.Message}");
                // TODO: publish failure event
            }
        }
    }
}