﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Events;
using Services.Common.Requests.Interfaces.Event;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Application.Handlers
{
    public class UserRegisteredEventHandler : IEventHandler<UserRegisteredEvent>
    {
        private readonly ILogger<UserRegisteredEventHandler> _logger;
        private readonly IWriteOnlyRepository<Folder> _writeOnlyFolderRepository;

        public UserRegisteredEventHandler(
            IWriteOnlyRepository<Folder> writeOnlyFolderRepository,
            ILogger<UserRegisteredEventHandler> logger
        )
        {
            _writeOnlyFolderRepository = writeOnlyFolderRepository;
            _logger = logger;
        }

        public async Task HandleAsync(UserRegisteredEvent @event)
        {
            try
            {
                var id = Guid.NewGuid();
                await _writeOnlyFolderRepository.Create(
                    new Folder
                    {
                        Id = id,
                        Files = null,
                        Name = "ROOT",
                        OwnerId = @event.UserId,
                        ParentId = id
                    },
                    CancellationToken.None
                );
                _logger.LogInformation($"Successfully created root folder for user {@event.UserId}.");
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Failed to create root folder for user {@event.UserId}. Reason: {e.Message}");
                // TODO: publish failure message
            }
        }
    }
}