﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Application.DTOs.Files;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Application.Queries.Files.ByFolder
{
    public class GetFolderItemsQuery : IRequest<FolderItems>
    {
        public GetFolderItemsQuery(Guid folderId, Guid ownerId)
        {
            FolderId = folderId;
            OwnerId = ownerId;
        }

        public Guid FolderId { get; }
        public Guid OwnerId { get; }
    }

    public class GetFolderItemsQueryHandler : IRequestHandler<GetFolderItemsQuery, FolderItems>
    {
        private readonly IReadOnlyRepository<File> _filesRepository;
        private readonly IReadOnlyRepository<Folder> _foldersRepository;
        private readonly ILogger<GetFolderItemsQueryHandler> _logger;

        public GetFolderItemsQueryHandler(IReadOnlyRepository<File> filesRepository,
            IReadOnlyRepository<Folder> foldersRepository, ILogger<GetFolderItemsQueryHandler> logger)
        {
            _filesRepository = filesRepository;
            _foldersRepository = foldersRepository;
            _logger = logger;
        }

        public async Task<FolderItems> Handle(GetFolderItemsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var files = await _filesRepository.GetAllByConditionAsync(
                    cancellationToken,
                    f => f.FolderId == request.FolderId && f.OwnerId == request.OwnerId,
                    i => i.Include(x => x.Folder)
                );

                var folders = await _foldersRepository.GetAllByConditionAsync(
                    cancellationToken,
                    f => f.ParentId == request.FolderId && f.ParentId != f.Id && f.OwnerId == request.OwnerId
                );

                var filesDto = files.Select(f => new FileDto
                {
                    Extension = f.Extension,
                    FolderDto = new FolderDto
                    {
                        Id = f.Folder.Id,
                        Name = f.Folder.Name,
                        ParentId = f.Folder.ParentId
                    },
                    Id = f.Id,
                    Name = f.FileName,
                    SizeInBytes = f.SizeInBytes
                }).ToList();

                var foldersDto = folders.Select(f => new FolderDto
                {
                    Id = f.Id,
                    Name = f.Name,
                    ParentId = f.ParentId
                }).ToList();

                var folder = await _foldersRepository.GetByConditionAsync(
                    cancellationToken,
                    f => f.Id == request.FolderId && f.OwnerId == request.OwnerId
                );

                var currentFolder = new FolderDto
                {
                    Id = folder.Id,
                    Name = folder.Name,
                    ParentId = folder.ParentId
                };

                _logger.LogInformation(
                    $"Found {filesDto.Count} files and {foldersDto.Count} folders for user {request.OwnerId} in folder {request.FolderId}.");
                return new FolderItems(filesDto, foldersDto, currentFolder);
            }
            catch (Exception e)
            {
                throw new QueryException(
                    $"Failed to retrieve the folder items for folder {request.FolderId} on behalf of user {request.OwnerId}.",
                    e);
            }
        }
    }
}