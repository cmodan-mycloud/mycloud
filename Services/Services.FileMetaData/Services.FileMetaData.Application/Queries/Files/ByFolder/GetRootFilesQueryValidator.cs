﻿using FluentValidation;

namespace Services.FileMetaData.Application.Queries.Files.ByFolder
{
    public class GetRootFilesQueryValidator : AbstractValidator<GetFolderItemsQuery>
    {
        public GetRootFilesQueryValidator()
        {
            RuleFor(p => p.FolderId).NotEmpty();
        }
    }
}