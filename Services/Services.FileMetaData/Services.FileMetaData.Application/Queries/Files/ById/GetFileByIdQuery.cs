﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Application.Queries.Files.ById
{
    public class GetFileByIdQuery : IRequest<File>
    {
        public GetFileByIdQuery(Guid fileId, Guid ownerId)
        {
            FileId = fileId;
            OwnerId = ownerId;
        }

        public Guid FileId { get; }
        public Guid OwnerId { get; }
    }

    public class GetFileByIdQueryHandler : IRequestHandler<GetFileByIdQuery, File>
    {
        private readonly ILogger<GetFileByIdQueryHandler> _logger;
        private readonly IReadOnlyRepository<File> _readOnlyRepository;

        public GetFileByIdQueryHandler(IReadOnlyRepository<File> readOnlyRepository,
            ILogger<GetFileByIdQueryHandler> logger)
        {
            _readOnlyRepository = readOnlyRepository;
            _logger = logger;
        }

        public async Task<File> Handle(GetFileByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var file = await _readOnlyRepository.GetByConditionAsync(
                    cancellationToken,
                    f => f.Id == request.FileId && f.OwnerId == request.OwnerId
                );
                _logger.LogInformation(
                    $"Successfully retrieved file metadata {request.FileId} on behalf of user {request.OwnerId}.");
                return file;
            }
            catch (Exception e)
            {
                throw new QueryException(
                    $"Failed to retrieve file metadata {request.FileId} on behalf of user {request.OwnerId}", e);
            }
        }
    }
}