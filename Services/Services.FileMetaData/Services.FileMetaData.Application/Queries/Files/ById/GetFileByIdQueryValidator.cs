﻿using FluentValidation;

namespace Services.FileMetaData.Application.Queries.Files.ById
{
    public class GetFolderByIdQueryValidator: AbstractValidator<GetFileByIdQuery>
    {
        public GetFolderByIdQueryValidator()
        {
            RuleFor(p => p.FileId).NotEmpty();
            RuleFor(p => p.OwnerId).NotEmpty();
        }
    }
}