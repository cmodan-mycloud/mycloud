﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Application.Queries.Files.Name
{
    public class GetFileNameByIdQuery : IRequest<string>
    {
        public GetFileNameByIdQuery(Guid fileId, Guid ownerId)
        {
            FileId = fileId;
            OwnerId = ownerId;
        }

        public Guid FileId { get; }
        public Guid OwnerId { get; }
    }

    public class GetFileNameByIdQueryHandler : IRequestHandler<GetFileNameByIdQuery, string>
    {
        private readonly IReadOnlyRepository<File> _fileRepository;
        private readonly ILogger<GetFileNameByIdQueryHandler> _logger;

        public GetFileNameByIdQueryHandler(IReadOnlyRepository<File> fileRepository,
            ILogger<GetFileNameByIdQueryHandler> logger)
        {
            _fileRepository = fileRepository;
            _logger = logger;
        }

        public async Task<string> Handle(GetFileNameByIdQuery request, CancellationToken cancellationToken)
        {
            var file = await _fileRepository.GetByConditionAsync(
                cancellationToken,
                f => f.Id == request.FileId && f.OwnerId == request.OwnerId
            );

            if (file != null)
            {
                _logger.LogInformation($"Successfully retrieved file {request.FileId} on behalf of {request.OwnerId}");
                return file.NewFileName;
            }

            _logger.LogError($"No file found with id {request.FileId} and owner {request.OwnerId}");
            throw new QueryException("No such file found!");
        }
    }
}