﻿using FluentValidation;

namespace Services.FileMetaData.Application.Queries.Files.Name
{
    public class GetFileNameByIdQueryValidator : AbstractValidator<GetFileNameByIdQuery>
    {
        public GetFileNameByIdQueryValidator()
        {
            RuleFor(p => p.FileId).NotEmpty();
            RuleFor(p => p.OwnerId).NotEmpty();
        }
    }
}