﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Application.DTOs.Files;
using Services.FileMetaData.Application.DTOs.Photos;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Application.Queries.Files.PhotosOnly
{
    public class GetPhotosOnlyQuery : IRequest<PhotoItems>
    {
        public GetPhotosOnlyQuery(Guid ownerId)
        {
            OwnerId = ownerId;
        }

        public Guid OwnerId { get; }
    }

    public class GetPhotosOnlyQueryHandler : IRequestHandler<GetPhotosOnlyQuery, PhotoItems>
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<GetPhotosOnlyQueryHandler> _logger;
        private readonly IReadOnlyRepository<File> _readOnlyRepository;

        public GetPhotosOnlyQueryHandler(IReadOnlyRepository<File> readOnlyRepository,
            ILogger<GetPhotosOnlyQueryHandler> logger, IConfiguration configuration)
        {
            _readOnlyRepository = readOnlyRepository;
            _logger = logger;
            _configuration = configuration;
        }

        public async Task<PhotoItems> Handle(GetPhotosOnlyQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var extensions = _configuration
                    .GetSection("photoExtensions")
                    .GetChildren()
                    .AsEnumerable()
                    .Select(e => e.Get<string>());

                var photos = await _readOnlyRepository.GetAllByConditionAsync(
                    cancellationToken,
                    f => f.OwnerId == request.OwnerId && extensions.Contains(f.Extension),
                    i => i.Include(p => p.Folder).OrderBy(p => p.CreatedOn)
                );

                var enumerable = photos != null ? photos.ToList() : new List<File>();

                _logger.LogInformation(
                    $"Successfully retrieved {enumerable.Count} photos on behalf of {request.OwnerId}.");

                var photoDtoS = enumerable.Select(f => new FileDto
                {
                    Extension = f.Extension,
                    FolderDto = new FolderDto
                    {
                        Id = f.Folder.Id,
                        Name = f.Folder.Name,
                        ParentId = f.Folder.ParentId
                    },
                    Id = f.Id,
                    Name = f.FileName,
                    SizeInBytes = f.SizeInBytes
                });

                return new PhotoItems(photoDtoS);
            }
            catch (Exception e)
            {
                throw new QueryException($"Failed to retrieve the photos on behalf of {request.OwnerId}.", e);
            }
        }
    }
}