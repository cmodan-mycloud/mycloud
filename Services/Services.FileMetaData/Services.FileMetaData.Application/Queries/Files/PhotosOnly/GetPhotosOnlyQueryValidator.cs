﻿using FluentValidation;

namespace Services.FileMetaData.Application.Queries.Files.PhotosOnly
{
    public class GetPhotosOnlyQueryValidator : AbstractValidator<GetPhotosOnlyQuery>
    {
        public GetPhotosOnlyQueryValidator()
        {
            RuleFor(p => p.OwnerId).NotEmpty();
        }
    }
}