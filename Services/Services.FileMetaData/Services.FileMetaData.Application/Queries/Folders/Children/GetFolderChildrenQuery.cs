﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Application.Queries.Folders.Children
{
    public class GetFolderChildrenQuery : IRequest<IEnumerable<Guid>>
    {
        public Guid FolderId { get; }
        public Guid OwnerId { get; }

        public GetFolderChildrenQuery(Guid folderId, Guid ownerId)
        {
            FolderId = folderId;
            OwnerId = ownerId;
        }
    }

    public class GetFolderChildrenQueryHandler : IRequestHandler<GetFolderChildrenQuery, IEnumerable<Guid>>
    {
        private readonly IReadOnlyRepository<Folder> _folderRepository;
        private readonly ILogger<GetFolderChildrenQueryHandler> _logger;

        public GetFolderChildrenQueryHandler(IReadOnlyRepository<Folder> folderRepository,
            ILogger<GetFolderChildrenQueryHandler> logger)
        {
            _folderRepository = folderRepository;
            _logger = logger;
        }

        public async Task<IEnumerable<Guid>> Handle(GetFolderChildrenQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var folders = await _folderRepository.GetAllByConditionAsync(
                    cancellationToken,
                    f => f.Id == request.FolderId && f.OwnerId == request.OwnerId && f.ParentId != f.Id
                );

                _logger.LogInformation(
                    $"Successfully retrieved the children folders of folder {request.FolderId} on behalf of user {request.OwnerId}.");
                return folders.Select(f => f.Id);
            }
            catch (Exception e)
            {
                throw new QueryException(
                    $"Failed to retrieve the children folders of folder {request.FolderId} on behalf of user {request.OwnerId}.", e);
            }
        }
    }
}