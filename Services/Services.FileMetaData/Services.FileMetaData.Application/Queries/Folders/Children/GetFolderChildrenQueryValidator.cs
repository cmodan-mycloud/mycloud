﻿using FluentValidation;

namespace Services.FileMetaData.Application.Queries.Folders.Children
{
    public class GetFolderChildrenQueryValidator: AbstractValidator<GetFolderChildrenQuery>
    {
        public GetFolderChildrenQueryValidator()
        {
            RuleFor(p => p.FolderId).NotEmpty();
            RuleFor(p => p.OwnerId).NotEmpty();
        }
    }
}