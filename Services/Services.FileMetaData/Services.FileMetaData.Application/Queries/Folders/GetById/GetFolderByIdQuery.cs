﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Application.Queries.Folders.GetById
{
    public class GetFolderByIdQuery : IRequest<Folder>
    {
        public GetFolderByIdQuery(Guid folderId, Guid ownerId)
        {
            FolderId = folderId;
            OwnerId = ownerId;
        }

        public Guid FolderId { get; }
        public Guid OwnerId { get; }
    }

    public class GetFolderByIdQueryHandler : IRequestHandler<GetFolderByIdQuery, Folder>
    {
        private readonly ILogger<GetFolderByIdQueryHandler> _logger;
        private readonly IReadOnlyRepository<Folder> _readOnlyRepository;

        public GetFolderByIdQueryHandler(IReadOnlyRepository<Folder> readOnlyRepository,
            ILogger<GetFolderByIdQueryHandler> logger)
        {
            _readOnlyRepository = readOnlyRepository;
            _logger = logger;
        }


        public async Task<Folder> Handle(GetFolderByIdQuery request,
            CancellationToken cancellationToken)
        {
            try
            {
                var folder = await _readOnlyRepository.GetByConditionAsync(
                    cancellationToken,
                    f => f.Id == request.FolderId && f.OwnerId == request.OwnerId
                );
                _logger.LogInformation($"Successfully retrieved folder {folder.Name}");
                return folder;
            }
            catch (Exception e)
            {
                throw new QueryException($"Failed to retrieve a folder for id {request.FolderId}.", e);
            }
        }
    }
}