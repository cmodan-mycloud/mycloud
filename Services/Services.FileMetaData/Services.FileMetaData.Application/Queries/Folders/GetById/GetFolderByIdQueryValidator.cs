﻿using FluentValidation;

namespace Services.FileMetaData.Application.Queries.Folders.GetById
{
    public class GetFolderByIdQueryValidator : AbstractValidator<GetFolderByIdQuery>
    {
        public GetFolderByIdQueryValidator()
        {
            RuleFor(p => p.FolderId).NotEmpty();
            RuleFor(p => p.OwnerId).NotEmpty();
        }
    }
}