﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Application.Queries.Folders.RootId
{
    public class GetRootFolderIdQuery : IRequest<Guid>
    {
        public GetRootFolderIdQuery(Guid ownerId)
        {
            OwnerId = ownerId;
        }

        public Guid OwnerId { get; }
    }

    public class GetRootFolderIdQueryHandler : IRequestHandler<GetRootFolderIdQuery, Guid>
    {
        private readonly ILogger<GetRootFolderIdQueryHandler> _logger;
        private readonly IReadOnlyRepository<Folder> _readOnlyFolderRepository;

        public GetRootFolderIdQueryHandler(
            IReadOnlyRepository<Folder> readOnlyFolderRepository,
            ILogger<GetRootFolderIdQueryHandler> logger
        )
        {
            _readOnlyFolderRepository = readOnlyFolderRepository;
            _logger = logger;
        }

        public async Task<Guid> Handle(GetRootFolderIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var folder = await _readOnlyFolderRepository.GetByConditionAsync(
                    cancellationToken,
                    f => f.OwnerId == request.OwnerId && f.ParentId == f.Id
                );
                _logger.LogInformation($"Found root folder for user {request.OwnerId}");
                return folder.Id;
            }
            catch (Exception e)
            {
                throw new QueryException("Root folder was not found!", e);
            }
        }
    }
}