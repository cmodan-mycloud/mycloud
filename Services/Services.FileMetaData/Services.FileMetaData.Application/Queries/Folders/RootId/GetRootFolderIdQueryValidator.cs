﻿using FluentValidation;

namespace Services.FileMetaData.Application.Queries.Folders.RootId
{
    public class GetRootFolderIdQueryValidator : AbstractValidator<GetRootFolderIdQuery>
    {
        public GetRootFolderIdQueryValidator()
        {
            RuleFor(p => p.OwnerId).NotEmpty();
        }
    }
}