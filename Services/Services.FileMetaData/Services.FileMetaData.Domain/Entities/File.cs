﻿using System;
using Services.Common.Domain.Entities;

namespace Services.FileMetaData.Domain.Entities
{
    public class File : IEntity
    {
        public Guid Id { get; set; }
        public Guid OwnerId { get; set; }
        public string FileName { get; set; }
        public string NewFileName { get; set; }
        public string Extension { get; set; }
        public long SizeInBytes { get; set; }
        public DateTime CreatedOn { get; set; }

        public Guid FolderId { get; set; }
        public Folder Folder { get; set; }
    }
}