﻿using System;
using System.Collections.Generic;
using Services.Common.Domain.Entities;

namespace Services.FileMetaData.Domain.Entities
{
    public class Folder : BaseEntity
    {
        public string Name { get; set; }
        public Guid OwnerId { get; set; }
        public Guid ParentId { get; set; }
        public DateTime CreatedOn { get; set; }

        public IEnumerable<File> Files { get; set; }
    }
}