﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Persistence.Configurations
{
    public class FileConfiguration : IEntityTypeConfiguration<File>
    {
        public void Configure(EntityTypeBuilder<File> builder)
        {
            builder.HasKey(e => e.Id);
            builder
                .HasOne(f => f.Folder)
                .WithMany(e => e.Files)
                .HasForeignKey(p => p.FolderId);
        }
    }
}