﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Persistence.Configurations
{
    public class FolderConfiguration : IEntityTypeConfiguration<Folder>
    {
        public void Configure(EntityTypeBuilder<Folder> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Name).IsRequired();
            builder
                .HasMany(f => f.Files)
                .WithOne(f => f.Folder)
                .HasForeignKey(p => p.FolderId);
        }
    }
}