﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services.Common.Infrastructure.Repositories;
using Services.Common.Requests.Interfaces.Repositories;
using Services.FileMetaData.Domain.Entities;

namespace Services.FileMetaData.Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddDbContext<FileMetaDataContext>(options => options.UseSqlServer(
                configuration.GetConnectionString("Files")), ServiceLifetime.Transient);

            // provide repositories
            services.AddTransient<IReadOnlyRepository<File>, ReadOnlyRepository<File>>(factory =>
                new ReadOnlyRepository<File>(factory.GetRequiredService<FileMetaDataContext>()));
            services.AddTransient<IWriteOnlyRepository<File>, WriteOnlyRepository<File>>(factory =>
                new WriteOnlyRepository<File>(factory.GetRequiredService<FileMetaDataContext>()));
            services.AddTransient<IReadOnlyRepository<Folder>, ReadOnlyRepository<Folder>>(factory =>
                new ReadOnlyRepository<Folder>(factory.GetRequiredService<FileMetaDataContext>()));
            services.AddTransient<IWriteOnlyRepository<Folder>, WriteOnlyRepository<Folder>>(factory =>
                new WriteOnlyRepository<Folder>(factory.GetRequiredService<FileMetaDataContext>()));
            return services;
        }
    }
}