﻿using Microsoft.EntityFrameworkCore;
using Services.FileMetaData.Persistence.Configurations;

namespace Services.FileMetaData.Persistence
{
    public class FileMetaDataContext : DbContext
    {
        public FileMetaDataContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Call the base
            base.OnModelCreating(modelBuilder);

            // Apply the configurations defined in 'Configurations'
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(FileConfiguration).Assembly);
        }
    }
}