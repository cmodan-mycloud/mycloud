﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Services.Common.Infrastructure.Persistence;

namespace Services.FileMetaData.Persistence
{
    /// <summary>
    /// Dynamically executed when the migrations are applied.
    /// </summary>
    public class FileMetaDataContextFactory : BaseDesignTimeFactory, IDesignTimeDbContextFactory<FileMetaDataContext>
    {
        public FileMetaDataContextFactory()
        {
            ConnectionStringName = "Files";
            ProjectName = "Services.FileMetaData.API";
        }

        public FileMetaDataContext CreateDbContext(string[] args)
        {
            var config = Configuration();

            var optionsBuilder = new DbContextOptionsBuilder<FileMetaDataContext>();
            optionsBuilder.UseSqlServer(
                config.GetConnectionString(ConnectionStringName),
                builder => builder.MigrationsAssembly(typeof(FileMetaDataContext).Assembly.FullName)
            );

            return new FileMetaDataContext(optionsBuilder.Options);
        }
    }
}