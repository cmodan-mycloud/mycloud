﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Services.FileMetaData.Persistence.Migrations
{
    public partial class File : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "File",
                table => new
                {
                    Id = table.Column<Guid>(),
                    OwnerId = table.Column<Guid>(),
                    FileName = table.Column<string>(nullable: true),
                    NewFileName = table.Column<string>(nullable: true),
                    Extension = table.Column<string>(nullable: true),
                    SizeInBytes = table.Column<long>()
                },
                constraints: table => { table.PrimaryKey("PK_File", x => x.Id); });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "File");
        }
    }
}