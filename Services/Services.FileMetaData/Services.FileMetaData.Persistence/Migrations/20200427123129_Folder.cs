﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Services.FileMetaData.Persistence.Migrations
{
    public partial class Folder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                "FolderId",
                "File",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                "Folder",
                table => new
                {
                    Id = table.Column<Guid>(),
                    Name = table.Column<string>(),
                    ParentId = table.Column<Guid>()
                },
                constraints: table => { table.PrimaryKey("PK_Folder", x => x.Id); });

            migrationBuilder.CreateIndex(
                "IX_File_FolderId",
                "File",
                "FolderId");

            migrationBuilder.AddForeignKey(
                "FK_File_Folder_FolderId",
                "File",
                "FolderId",
                "Folder",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                "FK_File_Folder_FolderId",
                "File");

            migrationBuilder.DropTable(
                "Folder");

            migrationBuilder.DropIndex(
                "IX_File_FolderId",
                "File");

            migrationBuilder.DropColumn(
                "FolderId",
                "File");
        }
    }
}