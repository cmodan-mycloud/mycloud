﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Services.FileMetaData.Persistence.Migrations
{
    public partial class FolderOwner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                "OwnerId",
                "Folder",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                "OwnerId",
                "Folder");
        }
    }
}