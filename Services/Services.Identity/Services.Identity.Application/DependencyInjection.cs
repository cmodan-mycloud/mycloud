﻿using System.Reflection;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Services.Common.Requests.Middlewares;

namespace Services.Identity.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                // add custom mappings from both the current assembly and the infrastructure
                cfg.AddMaps(Assembly.GetExecutingAssembly());
                cfg.AddMaps("Services.Identity.Infrastructure");
            });
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient<ValidationErrorsMiddleware>();
            services.AddTransient<QueryCommandExceptionMiddleware>();

            return services;
        }
    }
}