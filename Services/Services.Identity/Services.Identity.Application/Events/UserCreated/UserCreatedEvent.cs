﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Services.Common.Requests.Events;
using Services.Common.Requests.Interfaces.Publisher;
using Services.Identity.Domain.Entities;

namespace Services.Identity.Application.Events.UserCreated
{
    public class UserCreatedEvent : INotification
    {
        public UserCreatedEvent(ApplicationUser user)
        {
            User = user;
        }

        public ApplicationUser User { get; }
    }

    public class UserCreatedEventHandler : INotificationHandler<UserCreatedEvent>
    {
        private readonly IBusPublisher _busPublisher;

        public UserCreatedEventHandler(IBusPublisher busPublisher)
        {
            _busPublisher = busPublisher;
        }

        public async Task Handle(UserCreatedEvent notification, CancellationToken cancellationToken)
        {
            await _busPublisher.PublishAsync(new UserRegisteredEvent(notification.User.Id));
        }
    }
}