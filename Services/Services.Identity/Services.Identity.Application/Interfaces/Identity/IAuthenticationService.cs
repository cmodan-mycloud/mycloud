﻿using System.Threading.Tasks;
using Services.Common.Requests.Common;

namespace Services.Identity.Application.Interfaces.Identity
{
    public interface IAuthenticationService
    {
        public Task<Result> SignInAsync(string username, string password, string returnUrl);
        public Task<Result> SignOut(string returnUrl);
    }
}