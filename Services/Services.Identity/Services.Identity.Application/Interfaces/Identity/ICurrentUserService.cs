﻿using System.Threading;
using System.Threading.Tasks;
using Services.Identity.Domain.Entities;

namespace Services.Identity.Application.Interfaces.Identity
{
    public interface ICurrentUserService
    {
        public Task<ApplicationUser> CurrentUser(CancellationToken token);
    }
}