﻿using System;
using System.Threading.Tasks;
using Services.Common.Requests.Common;
using Services.Identity.Domain.Entities;

namespace Services.Identity.Application.Interfaces.Identity
{
    public interface IIdentityService
    {
        public Task<Result> CreateAsync(ApplicationUser applicationUser, string password);
        public Task<Result> DeleteAsync(ApplicationUser applicationUser);
        public Task<ApplicationUser> GetByIdAsync(Guid id);
        public Task<ApplicationUser> GetByEmailAsync(string email);
    }
}