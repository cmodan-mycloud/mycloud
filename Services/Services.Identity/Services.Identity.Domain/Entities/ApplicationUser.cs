﻿using System;
using Services.Common.Domain.Entities;

namespace Services.Identity.Domain.Entities
{
    public class ApplicationUser : IEntity
    {
        public ApplicationUser()
        {
        }

        public ApplicationUser(string firstName, string lastName, string email, string username)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            UserName = username;
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
    }
}