﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services.Common.Infrastructure.RabbitMq;
using Services.Common.Requests.Interfaces.Seeding;
using Services.Identity.Application.Interfaces.Identity;
using Services.Identity.Infrastructure.Identity;
using Services.Identity.Infrastructure.Persistence;
using Services.Identity.Infrastructure.Seeding;

namespace Services.Identity.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services,
            IConfiguration configuration)
        {
            // services.ConfigureApplicationCookie(config =>
            // {
            //     config.Cookie.Name = "IdentityServer.Cookie";
            //     config.LoginPath = "/Auth/Login";
            //     config.LogoutPath = "/Auth/Logout";
            // });

            services.AddDbContext<IdentityContext>(options =>
                {
                    options.UseSqlServer(configuration.GetConnectionString("Identity"), builder =>
                    {
                        builder.MigrationsAssembly(typeof(IdentityContext).Assembly.FullName);
                        builder.EnableRetryOnFailure();
                    });
                })
                .AddScoped<IDatabaseSeeder, DatabaseSeeder>();

            services.AddIdentity<User, IdentityRole<Guid>>(config =>
                {
                    config.Password.RequiredLength = 4;
                    config.Password.RequireDigit = false;
                    config.Password.RequireNonAlphanumeric = false;
                    config.Password.RequireUppercase = false;
                    config.User.RequireUniqueEmail = true;
                })
                .AddEntityFrameworkStores<IdentityContext>()
                .AddDefaultTokenProviders();

            services.AddScoped<IIdentityService, IdentityService>();
            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IDatabaseSeeder, DatabaseSeeder>();

            var connectionString = configuration.GetConnectionString("IdentityServer");
            var assembly = typeof(IdentityContext).Assembly.FullName;

            services.AddIdentityServer(builder =>
                {
                    builder.IssuerUri = configuration.GetSection("IssuerURI").Get<string>();
                })
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(connectionString,
                        sql => sql.MigrationsAssembly(assembly));
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(connectionString,
                        sql => sql.MigrationsAssembly(assembly));
                })
                .AddDeveloperSigningCredential()
                .AddAspNetIdentity<User>();

            services.AddCustomRawRabbit();

            return services;
        }
    }
}