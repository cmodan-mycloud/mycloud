﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Services.Common.Infrastructure.Persistence;
using Services.Identity.Infrastructure.Persistence;

namespace Services.Identity.Infrastructure.Factories
{
    public class ConfigurationContextDesignTimeFactory : BaseDesignTimeFactory,
        IDesignTimeDbContextFactory<ConfigurationDbContext>
    {
        public ConfigurationContextDesignTimeFactory()
        {
            ConnectionStringName = "IdentityServer";
            ProjectName = "Services.Identity.Web";
        }

        public ConfigurationDbContext CreateDbContext(string[] args)
        {
            var config = Configuration();

            var optionsBuilder = new DbContextOptionsBuilder<ConfigurationDbContext>();
            optionsBuilder.UseSqlServer(
                config.GetConnectionString(ConnectionStringName),
                builder =>
                {
                    builder.MigrationsAssembly(typeof(IdentityContext).Assembly.FullName);
                    builder.EnableRetryOnFailure();
                }
            );

            return new ConfigurationDbContext(optionsBuilder.Options, new ConfigurationStoreOptions());
        }
    }
}