﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Services.Common.Infrastructure.Persistence;
using Services.Identity.Infrastructure.Persistence;

namespace Services.Identity.Infrastructure.Factories
{
    class IdentityContextFactory : BaseDesignTimeFactory, IDesignTimeDbContextFactory<IdentityContext>
    {
        public IdentityContextFactory()
        {
            ConnectionStringName = "Identity";
            ProjectName = "Services.Identity.Web";
        }

        public IdentityContext CreateDbContext(string[] args)
        {
            var config = Configuration();

            var optionsBuilder = new DbContextOptionsBuilder<IdentityContext>();
            optionsBuilder.UseSqlServer(config.GetConnectionString(ConnectionStringName),
                builder =>
                {
                    builder.MigrationsAssembly(typeof(IdentityContext).Assembly.FullName);
                    builder.EnableRetryOnFailure();
                });

            var context = new IdentityContext(optionsBuilder.Options);
            return context;
        }
    }
}