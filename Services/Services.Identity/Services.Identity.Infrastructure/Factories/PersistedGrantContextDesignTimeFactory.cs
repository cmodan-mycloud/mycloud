﻿using System;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Services.Common.Infrastructure.Persistence;
using Services.Identity.Infrastructure.Persistence;

namespace Services.Identity.Infrastructure.Factories
{
    public class PersistedGrantContextDesignTimeFactory : BaseDesignTimeFactory,
        IDesignTimeDbContextFactory<PersistedGrantDbContext>
    {
        public PersistedGrantContextDesignTimeFactory()
        {
            ConnectionStringName = "IdentityServer";
            ProjectName = "Services.Identity.Web";
        }
        
        public PersistedGrantDbContext CreateDbContext(string[] args)
        {
            var config = Configuration();

            var optionsBuilder = new DbContextOptionsBuilder<PersistedGrantDbContext>();
            optionsBuilder.UseSqlServer(
                config.GetConnectionString(ConnectionStringName),
                builder =>
                {
                    builder.MigrationsAssembly(typeof(IdentityContext).Assembly.FullName);
                    builder.EnableRetryOnFailure();
                }
            );

            return new PersistedGrantDbContext(optionsBuilder.Options, new OperationalStoreOptions());
        }
    }
}