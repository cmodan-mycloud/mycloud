﻿using System.Threading;
using System.Threading.Tasks;
using IdentityServer4.Events;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using Services.Common.Requests.Common;
using Services.Identity.Application.Interfaces.Identity;

namespace Services.Identity.Infrastructure.Identity
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IEventService _eventService;
        private readonly IIdentityServerInteractionService _identityServerInteractionService;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;


        public AuthenticationService(
            SignInManager<User> signInManager,
            ICurrentUserService currentUserService,
            IIdentityServerInteractionService identityServerInteractionService,
            IEventService eventService,
            UserManager<User> userManager
        )
        {
            _signInManager = signInManager;
            _currentUserService = currentUserService;
            _identityServerInteractionService = identityServerInteractionService;
            _eventService = eventService;
            _userManager = userManager;
        }

        public async Task<Result> SignInAsync(string username, string password, string returnUrl)
        {
            var context = await _identityServerInteractionService.GetAuthorizationContextAsync(returnUrl);
            if (!IsValidUrl(returnUrl) || context == null)
            {
                await _eventService.RaiseAsync(new UserLoginFailureEvent(username, "invalid credentials"));
                return Result.Failure("Invalid credentials");
            }

            var user = await _userManager.FindByNameAsync(username);
            await _eventService.RaiseAsync(new UserLoginSuccessEvent(username, user.Id.ToString(), user.UserName,
                clientId: context.Client.ClientId));

            var result = await _signInManager.PasswordSignInAsync(username, password, true, true);
            return result.ToApplicationResult();
        }

        public async Task<Result> SignOut(string returnUrl)
        {
            var context = await _identityServerInteractionService.GetAuthorizationContextAsync(returnUrl);
            if (!IsValidUrl(returnUrl) || context == null)
            {
                return Result.Failure("Invalid return url");
            }

            var currentUser = await _currentUserService.CurrentUser(CancellationToken.None);
            if (currentUser == null) return Result.Failure("No authenticated user found");
            await _signInManager.SignOutAsync();
            await _eventService.RaiseAsync(new UserLogoutSuccessEvent(currentUser.Id.ToString(), "logout success"));
            return Result.Success();
        }

        private bool IsValidUrl(string url)
        {
            return _identityServerInteractionService.IsValidReturnUrl(url);
        }
    }
}