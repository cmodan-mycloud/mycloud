﻿using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Services.Identity.Application.Interfaces.Identity;
using Services.Identity.Domain.Entities;

namespace Services.Identity.Infrastructure.Identity
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly HttpContext _httpContext;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;

        public CurrentUserService(IHttpContextAccessor accessor, UserManager<User> userManager, IMapper mapper)
        {
            _mapper = mapper;
            _userManager = userManager;
            _httpContext = accessor.HttpContext;
        }

        public async Task<ApplicationUser> CurrentUser(CancellationToken token)
        {
            var id = _httpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var identityUser = await _userManager.FindByIdAsync(id);
            return _mapper.Map<ApplicationUser>(identityUser);
        }
    }
}