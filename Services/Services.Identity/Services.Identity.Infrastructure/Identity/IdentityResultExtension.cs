﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Services.Common.Requests.Common;

namespace Services.Identity.Infrastructure.Identity
{
    public static class IdentityResultExtension
    {
        public static Result ToApplicationResult(this IdentityResult result)
        {
            return result.Succeeded
                ? Result.Success()
                : Result.Failure(result.Errors.Select(e => e.Description));
        }

        public static Result ToApplicationResult(this SignInResult result)
        {
            if (result.Succeeded)
            {
                return Result.Success();
            }

            var errors = new List<string>();
            if (result.IsLockedOut)
            {
                errors.Add("The user has been locked out");
            }

            if (result.RequiresTwoFactor)
            {
                errors.Add("Two factor authentication is required.");
            }

            if (result.IsNotAllowed)
            {
                errors.Add("Incorrect credentials");
            }

            return Result.Failure(errors);
        }
    }
}