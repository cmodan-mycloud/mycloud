﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Services.Common.Requests.Common;
using Services.Identity.Application.Events.UserCreated;
using Services.Identity.Application.Interfaces.Identity;
using Services.Identity.Domain.Entities;

namespace Services.Identity.Infrastructure.Identity
{
    public class IdentityService : IIdentityService
    {
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;
        private readonly UserManager<User> _userManager;

        public IdentityService(UserManager<User> userManager, IMapper mapper, IMediator mediator)
        {
            _userManager = userManager;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<Result> CreateAsync(ApplicationUser applicationUser, string password)
        {
            var identityUser = _mapper.Map<User>(applicationUser);
            var identityResult = await _userManager.CreateAsync(identityUser, password);
            if (identityResult.Succeeded) await _mediator.Publish(new UserCreatedEvent(applicationUser));

            return identityResult.ToApplicationResult();
        }

        public async Task<Result> DeleteAsync(ApplicationUser applicationUser)
        {
            var identityUser = _mapper.Map<User>(applicationUser);
            var identityResult = await _userManager.DeleteAsync(identityUser);
            return identityResult.ToApplicationResult();
        }

        public async Task<ApplicationUser> GetByIdAsync(Guid id)
        {
            var identityUser = await _userManager.FindByIdAsync(id.ToString());
            return _mapper.Map<ApplicationUser>(identityUser);
        }

        public async Task<ApplicationUser> GetByEmailAsync(string email)
        {
            var identityUser = await _userManager.FindByEmailAsync(email);
            return _mapper.Map<ApplicationUser>(identityUser);
        }
    }
}