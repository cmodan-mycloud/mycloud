﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Services.Common.Domain.Entities;
using Services.Common.Requests.Mappings;
using Services.Identity.Domain.Entities;

namespace Services.Identity.Infrastructure.Identity
{
    public class User : IdentityUser<Guid>, IEntity, IMapFrom<ApplicationUser>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ApplicationUser, User>();
        }
    }
}