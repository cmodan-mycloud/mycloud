﻿using AutoMapper;
using Services.Identity.Domain.Entities;

namespace Services.Identity.Infrastructure.Identity
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<ApplicationUser, User>();
        }
    }
}