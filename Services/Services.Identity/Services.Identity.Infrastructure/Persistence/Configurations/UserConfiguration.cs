﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Services.Identity.Domain.Entities;

namespace Services.Identity.Infrastructure.Persistence.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.HasKey(p => p.Id);
            builder
                .Property(p => p.Email)
                .IsRequired();
            builder
                .Property(p => p.FirstName)
                .HasMaxLength(200)
                .IsRequired();
            builder
                .Property(p => p.LastName)
                .HasMaxLength(200)
                .IsRequired();
        }
    }
}