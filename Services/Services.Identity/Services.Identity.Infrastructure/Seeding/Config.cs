﻿using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;

namespace Services.Identity.Infrastructure.Seeding
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email()
            };
        }

        public static IEnumerable<ApiScope> GetScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope
                {
                    Name = "upload",
                    DisplayName = "Upload API",
                    Description = "Upload API"
                },
                new ApiScope
                {
                    Name = "download",
                    DisplayName = "Download API",
                    Description = "Download API"
                },
                new ApiScope
                {
                    Name = "metadata",
                    DisplayName = "MetaData API",
                    Description = "MetaData API"
                },
                new ApiScope
                {
                    Name = "notifications",
                    DisplayName = "Notifications API",
                    Description = "Notifications API"
                }
            };
        }

        public static IEnumerable<ApiResource> GetApis()
        {
            return new[]
            {
                new ApiResource("upload", "Upload API"),
                new ApiResource("download", "Download API"),
                new ApiResource("metadata", "MetaData API"),
                new ApiResource("notifications", "Notifications API")
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "spa",
                    AllowedGrantTypes =
                    {
                        GrantType.ResourceOwnerPassword
                    },
                    AllowedCorsOrigins =
                    {
                        "http://localhost:8100"
                    },
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = new List<string>
                    {
                        "upload", "download", "openid", "profile", "metadata", "notifications", "management"
                    },
                    AllowOfflineAccess = true
                },
                new Client
                {
                    ClientId = "Rest",

                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    RequirePkce = false,
                    RequireClientSecret = false,

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "upload", "download", "openid", "profile", "metadata", "notifications", "management"
                    },

                    AllowAccessTokensViaBrowser = false,
                    RequireConsent = false,
                    AllowOfflineAccess = false
                }
            };
        }
    }
}