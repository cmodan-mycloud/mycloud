﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Services.Common.Requests.Interfaces.Seeding;
using Services.Identity.Application.Interfaces.Identity;
using Services.Identity.Domain.Entities;
using Services.Identity.Infrastructure.Identity;
using Services.Identity.Infrastructure.Persistence;

namespace Services.Identity.Infrastructure.Seeding
{
    public class DatabaseSeeder : IDatabaseSeeder
    {
        private const string DummyPassword = "Password12345";
        private readonly ConfigurationDbContext _configurationContext;
        private readonly IdentityContext _identityContext;
        private readonly IIdentityService _identityService;


        public DatabaseSeeder(
            IdentityContext identityContext,
            ConfigurationDbContext configurationContext,
            IIdentityService identityService
        )
        {
            _identityContext = identityContext;
            _configurationContext = configurationContext;
            _identityService = identityService;
        }

        public async Task Seed()
        {
            await SeedIdentityUsers();
            await SeedClients();
        }

        private async Task SeedIdentityUsers()
        {
            if (_identityContext.Set<User>().Any()) return;

            // create the users
            var users = new[]
            {
                new ApplicationUser
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Eugen",
                    LastName = "Popescu",
                    Email = "eugen.popescu@email.com",
                    UserName = "eugen.popescu@email.com"
                },
                new ApplicationUser
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Samuel",
                    LastName = "Nica",
                    Email = "samuel.nica@email.com",
                    UserName = "samuel.nica@email.com"
                },
                new ApplicationUser
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Catalin",
                    LastName = "Modan",
                    Email = "catalin.modan@email.com",
                    UserName = "catalin.modan@email.com"
                }
            };

            foreach (var user in users) await _identityService.CreateAsync(user, DummyPassword);

            await _identityContext.SaveChangesAsync(CancellationToken.None);
        }

        private async Task SeedClients()
        {
            if (!_configurationContext.Clients.Any())
            {
                foreach (var client in Config.GetClients())
                    await _configurationContext.Clients.AddAsync(client.ToEntity());

                await _configurationContext.SaveChangesAsync();
            }

            if (!_configurationContext.IdentityResources.Any())
            {
                foreach (var resource in Config.GetIdentityResources())
                    await _configurationContext.IdentityResources.AddAsync(resource.ToEntity());

                await _configurationContext.SaveChangesAsync();
            }

            if (!_configurationContext.ApiResources.Any())
            {
                foreach (var resource in Config.GetApis())
                    await _configurationContext.ApiResources.AddAsync(resource.ToEntity());

                await _configurationContext.SaveChangesAsync();
            }

            if (!_configurationContext.ApiScopes.Any())
            {
                foreach (var resource in Config.GetScopes())
                    await _configurationContext.ApiScopes.AddAsync(resource.ToEntity());

                await _configurationContext.SaveChangesAsync();
            }
        }
    }
}