﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Common.Requests.Common;
using Services.Identity.Application.Interfaces.Identity;
using Services.Identity.Web.ViewModels.Login;
using Services.Identity.Web.ViewModels.Logout;

namespace Services.Identity.Web.Controllers
{
    [AllowAnonymous]
    public class AuthenticationController : BaseController
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(
            IAuthenticationService authenticationService
        )
        {
            _authenticationService = authenticationService;
        }

        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            return View(new LoginViewModel() {ReturnUrl = returnUrl});
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await _authenticationService.SignInAsync(model.Username, model.Password, model.ReturnUrl);
            if (result.Succeeded)
            {
                return Redirect(model.ReturnUrl);
            }

            AddModelErrors(result.Errors);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Logout(LogoutViewModel model)
        {
            var result = await _authenticationService.SignOut(model.ReturnUrl);
            if (result.Succeeded)
            {
                return Redirect(model.ReturnUrl);
            }

            return BadRequest(new ErrorResponse(400, result.Errors));
        }
    }
}