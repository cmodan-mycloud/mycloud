﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Services.Identity.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        protected void AddModelErrors(IEnumerable<string> errors)
        {
            foreach (var error in errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }

        protected void AddModelError(string error)
        {
            ModelState.AddModelError(string.Empty, error);
        }
    }
}