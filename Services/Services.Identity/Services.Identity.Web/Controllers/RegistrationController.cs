﻿using System.Threading.Tasks;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Mvc;
using Services.Identity.Application.Interfaces.Identity;
using Services.Identity.Domain.Entities;
using Services.Identity.Web.ViewModels.Login;
using Services.Identity.Web.ViewModels.Registration;

namespace Services.Identity.Web.Controllers
{
    public class RegistrationController : BaseController
    {
        private readonly IIdentityServerInteractionService _identityServerInteractionService;
        private readonly IIdentityService _identityService;

        public RegistrationController(IIdentityService identityService,
            IIdentityServerInteractionService identityServerInteractionService)
        {
            _identityService = identityService;
            _identityServerInteractionService = identityServerInteractionService;
        }

        [HttpGet]
        public IActionResult Register(string returnUrl)
        {
            return View(new RegisterViewModel() {ReturnUrl = returnUrl});
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!_identityServerInteractionService.IsValidReturnUrl(model.ReturnUrl))
            {
                AddModelError("Invalid return url");
                return View(model);
            }

            var applicationUser = new ApplicationUser(model.FirstName, model.LastName, model.Email, model.Email);
            var result = await _identityService.CreateAsync(applicationUser, model.Password);

            if (result.Succeeded)
                return RedirectToAction("Login", "Authentication", new LoginViewModel() {ReturnUrl = model.ReturnUrl});

            AddModelErrors(result.Errors);
            return View(model);
        }
    }
}