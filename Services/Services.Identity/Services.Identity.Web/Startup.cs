using Common.CORS;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Services.Identity.Application;
using Services.Identity.Infrastructure;

namespace Services.Identity.Web
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddCustomCors(_configuration)
                .AddApplication()
                .AddInfrastructure(_configuration)
                .AddHttpContextAccessor()
                .AddSingleton<IActionContextAccessor, ActionContextAccessor>()
                .AddControllersWithViews()
                .AddRazorRuntimeCompilation();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app
                .UseHttpsRedirection()
                .UseStaticFiles()
                .UseRouting()
                .UseCustomCors(_configuration)
                .UseIdentityServer()
                .UseEndpoints(endpoints => { endpoints.MapDefaultControllerRoute(); });
        }
    }
}