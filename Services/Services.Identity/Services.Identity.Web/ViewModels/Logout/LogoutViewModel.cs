﻿namespace Services.Identity.Web.ViewModels.Logout
{
    public class LogoutViewModel
    {
        public string ReturnUrl { get; set; }
    }
}