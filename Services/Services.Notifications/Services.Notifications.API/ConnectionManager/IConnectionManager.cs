﻿using System.Collections.Generic;

namespace Services.Notifications.API.ConnectionManager
{
    public interface IConnectionManager<T>
    {
        int Count { get; }
        void Add(T key, string connectionId);
        IEnumerable<string> GetConnections(T key);
        void Remove(T key, string connectionId);
    }
}