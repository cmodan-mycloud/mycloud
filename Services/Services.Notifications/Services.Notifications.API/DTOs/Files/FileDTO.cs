﻿using System;
using Services.Notifications.API.DTOs.Folder;

namespace Services.Notifications.API.DTOs.Files
{
    public class FileDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public long SizeInBytes { get; set; }
        public string Extension { get; set; }
        public FolderDto Folder { get; set; }
    }
}