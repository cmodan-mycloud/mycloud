﻿using System;

namespace Services.Notifications.API.DTOs.Folder
{
    public class FolderDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid ParentId { get; set; }
    }
}