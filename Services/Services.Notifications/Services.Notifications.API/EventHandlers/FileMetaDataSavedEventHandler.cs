﻿using System.Threading.Tasks;
using Services.Common.Requests.Events;
using Services.Common.Requests.Interfaces.Event;
using Services.Notifications.API.Hub.Files.Helper;
using Services.Notifications.API.Models.Files;

namespace Services.Notifications.API.EventHandlers
{
    public class FileMetaDataSavedEventHandler : IEventHandler<FileMetaDataSavedEvent>
    {
        private readonly IHandleFileNotifications _handleFileNotifications;

        public FileMetaDataSavedEventHandler(
            IHandleFileNotifications handleFileNotifications
        )
        {
            _handleFileNotifications = handleFileNotifications;
        }

        public async Task HandleAsync(FileMetaDataSavedEvent @event)
        {
            await _handleFileNotifications.PushFile(new FileUploadedNotificationModel(@event));
        }
    }
}