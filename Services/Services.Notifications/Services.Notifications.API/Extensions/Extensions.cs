﻿using System;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services.Common.Requests.Queries.UserId;
using Services.Notifications.API.ConnectionManager;
using Services.Notifications.API.Hub.Files.Helper;

namespace Services.Notifications.API.Extensions
{
    public static class MediatorExtensions
    {
        public static void AddCustomMediator(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMediatR(typeof(Startup));

            // custom handlers
            services.AddTransient<IRequestHandler<GetUserIdFromTokenQuery, Guid>, GetUserIdFromTokenQueryHandler>();
        }

        public static void AddCustomSignalR(this IServiceCollection services)
        {
            services.AddSignalR();
            services.AddSingleton<IConnectionManager<string>, ConnectionManager<string>>();
            services.AddTransient<IHandleFileNotifications, FilesHubHelper>();
        }
    }
}