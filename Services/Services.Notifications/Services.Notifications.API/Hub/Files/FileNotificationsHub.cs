﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Queries.UserId;
using Services.Notifications.API.ConnectionManager;

namespace Services.Notifications.API.Hub.Files
{
    [Authorize]
    public class FileNotificationsHub : Microsoft.AspNetCore.SignalR.Hub
    {
        private readonly IConnectionManager<string> _connectionManager;
        private readonly ILogger<FileNotificationsHub> _logger;
        private readonly IMediator _mediator;

        public FileNotificationsHub(
            IConnectionManager<string> connectionManager,
            IMediator mediator,
            ILogger<FileNotificationsHub> logger
        )
        {
            _connectionManager = connectionManager;
            _mediator = mediator;
            _logger = logger;
        }

        public override async Task OnConnectedAsync()
        {
            try
            {
                var userId = await _mediator.Send(new GetUserIdFromTokenQuery(Context.User.Claims));
                _connectionManager.Add(userId.ToString(), Context.ConnectionId);
                await base.OnConnectedAsync();
                _logger.LogInformation($"Successfully created a connection for client {userId}");
            }
            catch (Exception e)
            {
                _logger.LogError($"Failed to open a new connection. Reason: {e.Message}");
            }
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            try
            {
                var userId = await _mediator.Send(new GetUserIdFromTokenQuery(Context.User.Claims));
                _connectionManager.Remove(userId.ToString(), Context.ConnectionId);
                _logger.LogInformation($"Successfully disposed connection for client {userId}");
            }
            catch (Exception e)
            {
                _logger.LogError($"Failed to dispose existing connection. Reason: {e.Message}");
            }
        }
    }
}