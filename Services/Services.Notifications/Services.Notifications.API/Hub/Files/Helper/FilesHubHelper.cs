﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Services.Notifications.API.ConnectionManager;
using Services.Notifications.API.DTOs.Files;
using Services.Notifications.API.DTOs.Folder;
using Services.Notifications.API.Models.Files;
using Services.Notifications.API.Models.Response;

namespace Services.Notifications.API.Hub.Files.Helper
{
    public class FilesHubHelper : IHandleFileNotifications
    {
        private readonly IConnectionManager<string> _connectionManager;
        private readonly IHubContext<FileNotificationsHub> _hub;
        private readonly ILogger<FilesHubHelper> _logger;

        public FilesHubHelper(
            IHubContext<FileNotificationsHub> hub,
            IConnectionManager<string> connectionManager,
            ILogger<FilesHubHelper> logger
        )
        {
            _hub = hub;
            _connectionManager = connectionManager;
            _logger = logger;
        }

        public async Task PushFile(FileUploadedNotificationModel model)
        {
            var client = model.Payload.OwnerId.ToString();
            var clientConnections = _connectionManager.GetConnections(client);
            var payload = new FileDto
            {
                Extension = model.Payload.Extension,
                Id = model.Payload.Id,
                Folder = new FolderDto
                {
                    Id = model.Payload.Folder.Id,
                    Name = model.Payload.Folder.Name,
                    ParentId = model.Payload.Folder.ParentId
                },
                Name = model.Payload.Name,
                SizeInBytes = model.Payload.SizeBytes
            };

            foreach (var connection in clientConnections)
                await _hub.Clients.Client(connection).SendAsync("fileUploaded", new FileNotificationResponse(payload));
            _logger.LogInformation($"Successfully notified client {client}.");
        }
    }
}