﻿using System.Threading.Tasks;
using Services.Notifications.API.Models.Files;

namespace Services.Notifications.API.Hub.Files.Helper
{
    public interface IHandleFileNotifications
    {
        public Task PushFile(FileUploadedNotificationModel model);
    }
}