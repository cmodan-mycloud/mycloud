﻿using Services.Common.Requests.Events;

namespace Services.Notifications.API.Models.Files
{
    public class FileUploadedNotificationModel
    {
        public FileMetaDataSavedEvent Payload { get; }

        public FileUploadedNotificationModel(FileMetaDataSavedEvent payload)
        {
            Payload = payload;
        }
    }
}