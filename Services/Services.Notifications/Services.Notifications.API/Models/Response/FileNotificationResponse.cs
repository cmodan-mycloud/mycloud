﻿using Newtonsoft.Json;
using Services.Notifications.API.DTOs.Files;

namespace Services.Notifications.API.Models.Response
{
    public class FileNotificationResponse
    {
        [JsonConstructor]
        public FileNotificationResponse(FileDto payload)
        {
            Payload = payload;
        }

        public FileDto Payload { get; set; }
    }
}