using Common.CORS;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Services.Common.Infrastructure.RabbitMq;
using Services.Common.Requests.Events;
using Services.Common.Requests.Interfaces.Event;
using Services.Notifications.API.EventHandlers;
using Services.Notifications.API.Extensions;
using Services.Notifications.API.Hub.Files;

namespace Services.Notifications.API
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddJwtCustomAuthentication(_configuration);
            services.AddCustomCors(_configuration);
            services.AddCustomMediator(_configuration);

            // BUS client & event handlers
            services.AddCustomRawRabbit();
            services.AddTransient<IEventHandler<FileMetaDataSavedEvent>, FileMetaDataSavedEventHandler>();

            services.AddCustomSignalR();
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment environment)
        {
            if (environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCustomCors(_configuration);
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseRabbitMqSubscriber()
                .SubscribeEvent<FileMetaDataSavedEvent>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<FileNotificationsHub>("/files");
            });
        }
    }
}