﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Common.Requests.Queries.UserId;
using Services.Upload.Application.Commands.Files;
using Services.Upload.Models;

namespace Services.Upload.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class FileController : ControllerBase
    {
        private readonly IMediator _mediator;

        public FileController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        public async Task<IActionResult> Upload([FromForm] UploadRequestModel request)
        {
            // TODO: validate file size, extensions, etc. -> Fluent validation
            // TODO: check file -> security


            var userId = await _mediator.Send(new GetUserIdFromTokenQuery(HttpContext.User.Claims));
            await _mediator.Send(new UploadFileCommand(request.File, userId, request.FolderId));
            return Accepted();
        }
    }
}