﻿using System;
using Microsoft.AspNetCore.Http;

namespace Services.Upload.Models
{
    public class UploadRequestModel
    {
        /// <summary>
        ///     The array of files to be uploaded. Notice the set property, it is required for model binding.
        /// </summary>
        public IFormFile File { get; set; }

        public Guid FolderId { get; set; }
    }
}