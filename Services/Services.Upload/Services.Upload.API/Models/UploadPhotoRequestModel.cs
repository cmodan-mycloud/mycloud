﻿using Microsoft.AspNetCore.Http;

namespace Services.Upload.Models
{
    public class UploadPhotoRequestModel
    {
        public IFormFile Photo { get; set; }
    }
}