using Common.CORS;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Services.Common.Infrastructure.RabbitMq;
using Services.Common.Requests.Interfaces.IO.Models;
using Services.Common.Requests.Middlewares;
using Services.Upload.Application;
using Services.Upload.Application.Commands.Files;

namespace Services.Upload
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddJwtCustomAuthentication(_configuration)
                .AddCustomCors(_configuration)
                .Configure<LocalStoragePath>(_configuration.GetSection("storage"))
                .AddApplication()
                .AddControllers()
                .AddFluentValidation(options =>
                {
                    options.RegisterValidatorsFromAssemblyContaining<UploadFileCommandValidator>();
                });
            services.AddCustomRawRabbit();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment environment)
        {
            if (environment.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseCustomCors(_configuration);
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseMiddleware<ValidationErrorsMiddleware>();
            app.UseMiddleware<QueryCommandExceptionMiddleware>();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}