﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Services.Common.Requests.Exceptions;
using Services.Common.Requests.Interfaces.IO;
using Services.Common.Requests.Interfaces.IO.Models;
using Services.Upload.Application.Events;
using Services.Upload.Common.Upload;

namespace Services.Upload.Application.Commands.Files
{
    public class UploadFileCommand : IRequest
    {
        public UploadFileCommand(IFormFile file, Guid ownerId, Guid folderId)
        {
            File = file;
            OwnerId = ownerId;
            FolderId = folderId;
        }

        public IFormFile File { get; }
        public Guid OwnerId { get; }
        public Guid FolderId { get; }
    }

    public class UploadFileCommandHandler : IRequestHandler<UploadFileCommand, Unit>
    {
        private readonly IExtractExtension _extractExtension;
        private readonly IHandleFileName _handleFileName;
        private readonly ILogger<UploadFileCommandHandler> _logger;
        private readonly IMediator _mediator;
        private readonly IPathHelper _pathHelper;
        private readonly IUploadManager _uploadManager;

        public UploadFileCommandHandler(
            IPathHelper pathHelper,
            IUploadManager uploadManager,
            IMediator mediator,
            ILogger<UploadFileCommandHandler> logger,
            IExtractExtension extractExtension, IHandleFileName handleFileName)
        {
            _pathHelper = pathHelper;
            _uploadManager = uploadManager;
            _mediator = mediator;
            _logger = logger;
            _extractExtension = extractExtension;
            _handleFileName = handleFileName;
        }

        public async Task<Unit> Handle(UploadFileCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var userDataDirectory =
                    await _pathHelper.GetOnBehalfOf(new DirectoryPathModel(request.OwnerId), cancellationToken);
                var diskName = await _uploadManager.Upload(new UploadModel(request.File, userDataDirectory),
                    cancellationToken);
                var extension = await _extractExtension.ExtractExtensionFrom(request.File.FileName, cancellationToken);
                var name = _handleFileName.ExtractName(request.File.FileName);

                await _mediator.Publish(
                    new FileSavedEvent(
                        name,
                        request.File.Length,
                        extension,
                        request.OwnerId,
                        diskName,
                        request.FolderId
                    ), cancellationToken);
                _logger.LogInformation(
                    $"Successfully uploaded {request.File.FileName} on behalf of user {request.OwnerId}.");
            }
            catch (Exception e)
            {
                throw new CommandException(
                    $"Failed to upload {request.File.FileName} on behalf of user {request.OwnerId}.", e);
            }

            return Unit.Value;
        }
    }
}