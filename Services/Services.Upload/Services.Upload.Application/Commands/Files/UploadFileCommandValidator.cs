﻿using FluentValidation;

namespace Services.Upload.Application.Commands.Files
{
    public class UploadFileCommandValidator : AbstractValidator<UploadFileCommand>
    {
        public UploadFileCommandValidator()
        {
            RuleFor(u => u.File).NotEmpty();
            RuleFor(u => u.OwnerId).NotEmpty();
        }
    }
}