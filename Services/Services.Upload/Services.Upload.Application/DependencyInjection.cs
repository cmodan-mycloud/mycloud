﻿using System;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Services.Common.Infrastructure;
using Services.Common.Infrastructure.IO;
using Services.Common.Requests.Behaviors.Validation;
using Services.Common.Requests.Interfaces.IO;
using Services.Common.Requests.Interfaces.Utilities;
using Services.Common.Requests.Middlewares;
using Services.Common.Requests.Queries.UserId;
using Services.Upload.Application.Commands.Files;
using Services.Upload.Application.Upload;
using Services.Upload.Common.Upload;

namespace Services.Upload.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddMediatR(typeof(UploadFileCommand));
            services.AddTransient<IRequestHandler<GetUserIdFromTokenQuery, Guid>, GetUserIdFromTokenQueryHandler>();
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));

            services.AddTransient<IPathHelper, FilesystemPathHelper>();
            services.AddTransient<IUploadManager, LocalStorageUploadManager>();
            services.AddSingleton<IGuessPlatform, PlatformGuesser>();
            services.AddTransient<IManipulatePaths, PhysicalStoragePathManipulator>();
            services.AddTransient<IExtractExtension, ExtensionExtractor>();
            services.AddTransient<ICreateUniqueName, DateTimeNameCreator>();
            services.AddTransient<IHandleFileName, FileNameExtractor>();
            services.AddTransient<ValidationErrorsMiddleware>();
            services.AddTransient<QueryCommandExceptionMiddleware>();
            return services;
        }
    }
}