﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Services.Common.Requests.Events;
using Services.Common.Requests.Interfaces.Publisher;

namespace Services.Upload.Application.Events
{
    public class FileSavedEvent : INotification
    {
        public FileSavedEvent(string fileName, long sizeInBytes, string extension, Guid ownerId, string diskName,
            Guid folderId)
        {
            FileName = fileName;
            SizeInBytes = sizeInBytes;
            Extension = extension;
            OwnerId = ownerId;
            DiskName = diskName;
            FolderId = folderId;
        }

        public string FileName { get; }
        public long SizeInBytes { get; }
        public string Extension { get; }
        public Guid OwnerId { get; }
        public string DiskName { get; }
        public Guid FolderId { get; }
    }

    public class FileSavedEventHandler : INotificationHandler<FileSavedEvent>
    {
        private readonly IBusPublisher _busPublisher;

        public FileSavedEventHandler(IBusPublisher busPublisher)
        {
            _busPublisher = busPublisher;
        }

        public async Task Handle(FileSavedEvent notification, CancellationToken cancellationToken)
        {
            await _busPublisher.PublishAsync(
                new FileUploadedEvent(
                    notification.OwnerId,
                    notification.FileName,
                    notification.DiskName,
                    notification.Extension,
                    notification.SizeInBytes,
                    notification.FolderId
                )
            );
        }
    }
}