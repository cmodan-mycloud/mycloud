﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Services.Common.Requests.Events;
using Services.Common.Requests.Interfaces.Publisher;

namespace Services.Upload.Application.Events
{
    public class PhotoSavedEvent : INotification
    {
        public PhotoSavedEvent(string fileName, long sizeInBytes, string extension, Guid ownerId, string diskName)
        {
            FileName = fileName;
            SizeInBytes = sizeInBytes;
            Extension = extension;
            OwnerId = ownerId;
            DiskName = diskName;
        }

        public string FileName { get; }
        public long SizeInBytes { get; }
        public string Extension { get; }
        public Guid OwnerId { get; }
        public string DiskName { get; }
    }

    public class PhotoSavedEventHandler : INotificationHandler<PhotoSavedEvent>
    {
        private readonly IBusPublisher _busPublisher;

        public PhotoSavedEventHandler(IBusPublisher busPublisher)
        {
            _busPublisher = busPublisher;
        }

        public async Task Handle(PhotoSavedEvent notification, CancellationToken cancellationToken)
        {
            await _busPublisher.PublishAsync(
                new PhotoUploadedEvent(
                    notification.FileName,
                    notification.SizeInBytes,
                    notification.Extension,
                    notification.OwnerId,
                    notification.DiskName
                )
            );
        }
    }
}