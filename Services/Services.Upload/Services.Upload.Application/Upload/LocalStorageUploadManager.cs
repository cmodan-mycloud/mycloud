﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Services.Common.Requests.Interfaces.IO;
using Services.Common.Requests.Interfaces.Utilities;
using Services.Upload.Common.Upload;

namespace Services.Upload.Application.Upload
{
    public class LocalStorageUploadManager : IUploadManager
    {
        private readonly ICreateUniqueName _createUniqueName;
        private readonly IExtractExtension _extractExtension;
        private readonly IManipulatePaths _manipulatePaths;

        public LocalStorageUploadManager(
            IExtractExtension extractExtension,
            ICreateUniqueName uniqueName,
            IManipulatePaths manipulatePaths
        )
        {
            _extractExtension = extractExtension;
            _createUniqueName = uniqueName;
            _manipulatePaths = manipulatePaths;
        }

        public async Task<string> Upload(UploadModel model, CancellationToken cancellationToken)
        {
            var extension = await _extractExtension.ExtractExtensionFrom(model.File.FileName, cancellationToken);
            var newFileName = await _createUniqueName.Create(extension, cancellationToken);

            if (!await _manipulatePaths.DirectoryExists(model.Directory, cancellationToken))
                await _manipulatePaths.CreateDirectory(model.Directory, cancellationToken);

            var fileUploadLocation = await _manipulatePaths.Combine(model.Directory, newFileName, cancellationToken);
            await using var stream = new FileStream(fileUploadLocation, FileMode.Create);
            await model.File.CopyToAsync(stream, cancellationToken);
            return newFileName;
        }
    }
}