﻿using System.Threading;
using System.Threading.Tasks;

namespace Services.Upload.Common.Upload
{
    public interface IUploadManager
    {
        public Task<string> Upload(UploadModel model, CancellationToken cancellationToken);
    }
}