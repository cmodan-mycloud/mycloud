﻿using Microsoft.AspNetCore.Http;

namespace Services.Upload.Common.Upload
{
    public class UploadModel
    {
        public UploadModel(IFormFile file, string directory)
        {
            File = file;
            Directory = directory;
        }

        public IFormFile File { get; }
        public string Directory { get; }
    }
}